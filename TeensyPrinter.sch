EESchema Schematic File Version 4
LIBS:TeensyPrinter-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 10
Title "Teensy Pololu Shield"
Date "2019-03-28"
Rev "0.2"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 8150 650  1100 550 
U 5C876DD3
F0 "Axis Steppers" 50
F1 "steppers.sch" 50
$EndSheet
$Sheet
S 9350 650  1100 550 
U 5C897698
F0 "Extruder Steppers" 50
F1 "ext_steppers.sch" 50
$EndSheet
Text GLabel 3000 3200 0    50   Output ~ 0
SD_SDI
Text GLabel 3000 3300 0    50   Input ~ 0
SD_SDO
Text GLabel 3000 5900 0    50   Output ~ 0
SD_SCK
$Sheet
S 9350 2900 1100 550 
U 5C7EEDF9
F0 "Display Adapter" 50
F1 "display.sch" 50
$EndSheet
Text GLabel 5000 2000 2    50   Input ~ 0
X_MIN
Text GLabel 5000 2200 2    50   Input ~ 0
Y_MIN
Text GLabel 3000 6100 0    50   Input ~ 0
Z_MIN
Text GLabel 3000 2800 0    50   Output ~ 0
X_STEP
Text GLabel 3000 2700 0    50   Output ~ 0
X_DIR
Text GLabel 3000 3500 0    50   Output ~ 0
X_EN
Text GLabel 3000 3000 0    50   Output ~ 0
Y_STEP
Text GLabel 3000 2900 0    50   Output ~ 0
Y_DIR
Text GLabel 3000 3600 0    50   Output ~ 0
Y_EN
Text GLabel 3000 4100 0    50   Output ~ 0
Z_STEP
Text GLabel 3000 3100 0    50   Output ~ 0
Z_DIR
Text GLabel 3000 3700 0    50   Output ~ 0
Z_EN
Text GLabel 3000 4200 0    50   Output ~ 0
E0_STEP
Text GLabel 3000 4300 0    50   Output ~ 0
E0_DIR
Text GLabel 3000 3800 0    50   Output ~ 0
E0_EN
Text GLabel 3000 5000 0    50   Output ~ 0
E1_STEP
Text GLabel 3000 4900 0    50   Output ~ 0
E1_DIR
Text GLabel 3000 3900 0    50   Output ~ 0
E1_EN
Text GLabel 5000 4900 2    50   Input ~ 0
THERM_E0
Text GLabel 5000 4700 2    50   Input ~ 0
THERM_E1
Text GLabel 5000 5000 2    50   Input ~ 0
THERM_BED
Text GLabel 5000 6200 2    50   Output ~ 0
FAN
Text GLabel 5000 3300 2    50   Input ~ 0
BTN_ENC
Text GLabel 5000 3500 2    50   Input ~ 0
BTN_EN1
Text GLabel 5000 3400 2    50   Input ~ 0
BTN_EN2
Text GLabel 6050 3900 2    50   Output ~ 0
LCD_RS
Text GLabel 5000 3800 2    50   Output ~ 0
LCD_EN
Text GLabel 5000 3700 2    50   Output ~ 0
LCD_D4
Text GLabel 5000 2600 2    50   Output ~ 0
LCD_D5
Text GLabel 5000 2500 2    50   Output ~ 0
LCD_D6
Text GLabel 5000 2400 2    50   Output ~ 0
LCD_D7
Text GLabel 2000 2100 0    50   Output ~ 0
X_CS
Text GLabel 1800 2300 2    50   Output ~ 0
Y_CS
Text GLabel 1800 2600 2    50   Output ~ 0
Z_CS
Text GLabel 1800 2900 2    50   Output ~ 0
E0_CS
Text GLabel 1800 3200 2    50   Output ~ 0
E1_CS
Text GLabel 5000 2800 2    50   Output ~ 0
TMC_SDI
Text GLabel 5000 2900 2    50   Input ~ 0
TMC_SDO
Text GLabel 6100 3000 2    50   Output ~ 0
TMC_SCK
Text GLabel 3000 5200 0    50   Output ~ 0
E2_STEP
Text GLabel 3000 5100 0    50   Output ~ 0
E2_DIR
Text GLabel 3000 4000 0    50   Output ~ 0
E2_EN
Text GLabel 1800 3500 2    50   Output ~ 0
E2_CS
Text GLabel 5000 4600 2    50   Input ~ 0
THERM_E2
Text GLabel 5000 2100 2    50   Input ~ 0
X_MAX
Text GLabel 5000 2300 2    50   Input ~ 0
Y_MAX
Text GLabel 3000 6000 0    50   Input ~ 0
Z_MAX
Text GLabel 5000 2700 2    50   Output ~ 0
BEEPER
Text GLabel 5000 3600 2    50   Input ~ 0
SD_DET
Text GLabel 3000 6200 0    50   Output ~ 0
HEATER_E2
Text GLabel 3000 5700 0    50   Input ~ 0
FILWIDTH
Text GLabel 3000 5600 0    50   Input ~ 0
THERM_ENCL
Text GLabel 3000 5300 0    50   Output ~ 0
SERVO_1
Text GLabel 3000 5400 0    50   Output ~ 0
SERVO_0
Text GLabel 3000 5500 0    50   Output ~ 0
PS_ON
Text GLabel 5000 6100 2    50   Output ~ 0
SUICIDE
Text GLabel 5000 6000 2    50   Output ~ 0
SOL_0
Text GLabel 5950 5900 2    50   Output ~ 0
SD_CS
$Comp
L power:GND #PWR02
U 1 1 5C97DCEB
P 2400 6450
F 0 "#PWR02" H 2400 6200 50  0001 C CNN
F 1 "GND" H 2405 6277 50  0000 C CNN
F 2 "" H 2400 6450 50  0001 C CNN
F 3 "" H 2400 6450 50  0001 C CNN
	1    2400 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 6450 2400 5800
Wire Wire Line
	2400 5800 3000 5800
Connection ~ 2400 5800
Wire Wire Line
	2400 2000 3000 2000
$Comp
L power:GND #PWR08
U 1 1 5C97E3F0
P 5700 6450
F 0 "#PWR08" H 5700 6200 50  0001 C CNN
F 1 "GND" H 5705 6277 50  0000 C CNN
F 2 "" H 5700 6450 50  0001 C CNN
F 3 "" H 5700 6450 50  0001 C CNN
	1    5700 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 6450 5700 5400
Wire Wire Line
	5700 5400 5000 5400
Wire Wire Line
	5700 5400 5700 4500
Connection ~ 5700 5400
Wire Wire Line
	5700 4500 5700 4400
Connection ~ 5700 4500
Wire Wire Line
	5700 4400 5700 3200
Connection ~ 5700 4400
Wire Wire Line
	5000 5300 5800 5300
Wire Wire Line
	5800 5300 5800 1750
$Comp
L power:+5V #PWR01
U 1 1 5C980CB4
P 5800 1750
F 0 "#PWR01" H 5800 1600 50  0001 C CNN
F 1 "+5V" H 5815 1923 50  0000 C CNN
F 2 "" H 5800 1750 50  0001 C CNN
F 3 "" H 5800 1750 50  0001 C CNN
	1    5800 1750
	1    0    0    -1  
$EndComp
$Sheet
S 8150 2150 1100 550 
U 5C9B0890
F0 "Endstops" 50
F1 "endstops.sch" 50
$EndSheet
$Sheet
S 9350 1400 1100 550 
U 5CA6156B
F0 "Thermistors" 50
F1 "thermistors.sch" 50
$EndSheet
$Sheet
S 8150 1400 1100 550 
U 5CAA1580
F0 "Heater MOSFETs" 50
F1 "heaters.sch" 50
$EndSheet
$Sheet
S 9350 2150 1100 550 
U 5CAA15C8
F0 "Miscellaneous I/O" 50
F1 "misc.sch" 50
$EndSheet
$Sheet
S 8150 3650 1100 550 
U 5CAF2CE7
F0 "Power Supply" 50
F1 "pwr_supply.sch" 50
$EndSheet
NoConn ~ 3000 3400
NoConn ~ 5000 5500
NoConn ~ 5000 5200
NoConn ~ 5000 5100
NoConn ~ 5000 4800
NoConn ~ 5000 4300
NoConn ~ 5000 4200
NoConn ~ 5000 4100
NoConn ~ 5000 4000
NoConn ~ 5000 3100
$Sheet
S 8150 2900 1100 550 
U 5CB1DC3F
F0 "Emergency Stop" 50
F1 "e-stop.sch" 50
$EndSheet
$Comp
L power:+3.3V #PWR0120
U 1 1 5CC582CE
P 5950 3600
F 0 "#PWR0120" H 5950 3450 50  0001 C CNN
F 1 "+3.3V" H 5965 3773 50  0000 C CNN
F 2 "" H 5950 3600 50  0001 C CNN
F 3 "" H 5950 3600 50  0001 C CNN
	1    5950 3600
	1    0    0    -1  
$EndComp
Text GLabel 5000 5800 2    50   Output ~ 0
HEATER_E1
Text GLabel 5000 5700 2    50   Output ~ 0
HEATER_E0
Text GLabel 5000 5600 2    50   Output ~ 0
HEATER_BED
Wire Wire Line
	5700 4500 5000 4500
Wire Wire Line
	5700 4400 5000 4400
Wire Wire Line
	5700 3200 5000 3200
$Comp
L teensy:Teensy3.6-NoProg U1
U 1 1 5C7F3D5D
P 4000 4100
F 0 "U1" H 4000 6450 60  0000 C CNN
F 1 "Teensy3.6" H 4000 1800 60  0000 C CNN
F 2 "teensy:Teensy35_36-NoProg" H 4000 4100 60  0001 C CNN
F 3 "" H 4000 4100 60  0000 C CNN
F 4 "Samtec Inc." H 550 50  50  0001 C CNN "DK_Mfr"
F 5 "SSA-132-S-T" H 550 50  50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM1122-32-ND" H 550 50  50  0001 C CNN "DK_PN"
F 7 "3" H 550 50  50  0001 C CNN "Qty Per Unit"
F 8 "any" H 550 50  50  0001 C CNN "Src Any/Spec"
	1    4000 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 2000 2400 5800
$Comp
L Connector_Generic:Conn_01x03 J40
U 1 1 5CAF1276
P 1600 2300
F 0 "J40" H 1750 2300 50  0000 C CNN
F 1 "Conn_01x03" H 1518 2526 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1600 2300 50  0001 C CNN
F 3 "~" H 1600 2300 50  0001 C CNN
	1    1600 2300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3000 2200 1800 2200
Wire Wire Line
	1800 2400 2450 2400
Wire Wire Line
	2450 2400 2450 2100
Connection ~ 2450 2100
Wire Wire Line
	2450 2100 3000 2100
$Comp
L Connector_Generic:Conn_01x03 J41
U 1 1 5CAF9031
P 1600 2600
F 0 "J41" H 1750 2600 50  0000 C CNN
F 1 "Conn_01x03" H 1518 2826 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1600 2600 50  0001 C CNN
F 3 "~" H 1600 2600 50  0001 C CNN
	1    1600 2600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1800 2500 2150 2500
Wire Wire Line
	2150 2500 2150 2300
Wire Wire Line
	2150 2300 3000 2300
Wire Wire Line
	2450 2400 2450 2700
Wire Wire Line
	2450 2700 1800 2700
Connection ~ 2450 2400
$Comp
L Connector_Generic:Conn_01x03 J42
U 1 1 5CAFC4CA
P 1600 2900
F 0 "J42" H 1750 2900 50  0000 C CNN
F 1 "Conn_01x03" H 1518 3126 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1600 2900 50  0001 C CNN
F 3 "~" H 1600 2900 50  0001 C CNN
	1    1600 2900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1800 2800 2550 2800
Wire Wire Line
	2550 2800 2550 2400
Wire Wire Line
	2550 2400 3000 2400
Wire Wire Line
	2450 2700 2450 3000
Wire Wire Line
	2450 3000 1800 3000
Connection ~ 2450 2700
$Comp
L Connector_Generic:Conn_01x03 J43
U 1 1 5CAFE94F
P 1600 3200
F 0 "J43" H 1750 3200 50  0000 C CNN
F 1 "Conn_01x03" H 1518 3426 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1600 3200 50  0001 C CNN
F 3 "~" H 1600 3200 50  0001 C CNN
	1    1600 3200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1800 3100 2250 3100
Wire Wire Line
	2250 3100 2250 2500
Wire Wire Line
	2250 2500 3000 2500
Wire Wire Line
	2450 3000 2450 3300
Wire Wire Line
	2450 3300 1800 3300
Connection ~ 2450 3000
Wire Wire Line
	2000 2100 2450 2100
$Comp
L Connector_Generic:Conn_01x03 J44
U 1 1 5CB02885
P 1600 3500
F 0 "J44" H 1750 3500 50  0000 C CNN
F 1 "Conn_01x03" H 1518 3726 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1600 3500 50  0001 C CNN
F 3 "~" H 1600 3500 50  0001 C CNN
	1    1600 3500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1800 3400 2350 3400
Wire Wire Line
	2350 3400 2350 2600
Wire Wire Line
	2350 2600 3000 2600
Wire Wire Line
	1800 3600 2450 3600
Wire Wire Line
	2450 3600 2450 3300
Connection ~ 2450 3300
$Comp
L Device:R_US R13
U 1 1 5CB0D6AD
P 5950 3750
F 0 "R13" H 6018 3796 50  0000 L CNN
F 1 "10k" H 6018 3705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5990 3740 50  0001 C CNN
F 3 "~" H 5950 3750 50  0001 C CNN
	1    5950 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R14
U 1 1 5CB0DCE2
P 5850 5750
F 0 "R14" H 5918 5796 50  0000 L CNN
F 1 "10k" H 5918 5705 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5890 5740 50  0001 C CNN
F 3 "~" H 5850 5750 50  0001 C CNN
	1    5850 5750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR050
U 1 1 5CB0E788
P 5850 5600
F 0 "#PWR050" H 5850 5450 50  0001 C CNN
F 1 "+3.3V" H 5865 5773 50  0000 C CNN
F 2 "" H 5850 5600 50  0001 C CNN
F 3 "" H 5850 5600 50  0001 C CNN
	1    5850 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 5900 5850 5900
Wire Wire Line
	5000 3900 5950 3900
Connection ~ 5950 3900
Wire Wire Line
	5950 3900 6050 3900
Connection ~ 5850 5900
Wire Wire Line
	5850 5900 5950 5900
Text Notes 1500 1800 0    50   ~ 0
J40-J54 usage for TMC2130 SPI:\nall 1-2: star configuration \n         (required for Marlin)\nall 2-3: daisy-chain configuration\n         (future use?)\n\nIn daisy-chain configuration, five \nmore GPIOs are available on pin 1 \non J40-J44.\n\nif TMC2130s are not used, four\nmore GPIOs are available on pin 3 \non J40 and pins 2, 4, and 6 on the \nSPI connector.
Wire Wire Line
	5000 3000 6100 3000
$Comp
L Mechanical:MountingHole H1
U 1 1 5CBB1194
P 8300 5900
F 0 "H1" H 8400 5946 50  0000 L CNN
F 1 "Lumbergh" H 8400 5855 50  0000 L CNN
F 2 "images:lumbergh" H 8300 5900 50  0001 C CNN
F 3 "~" H 8300 5900 50  0001 C CNN
	1    8300 5900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5CBB1C84
P 8300 6200
F 0 "H4" H 8400 6246 50  0000 L CNN
F 1 "OSHW" H 8400 6155 50  0000 L CNN
F 2 "Symbol:OSHW-Symbol_13.4x12mm_SilkScreen" H 8300 6200 50  0001 C CNN
F 3 "~" H 8300 6200 50  0001 C CNN
	1    8300 6200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 5CBB1E3E
P 8850 6200
F 0 "H5" H 8950 6246 50  0000 L CNN
F 1 "M3_Hole" H 8950 6155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 8850 6200 50  0001 C CNN
F 3 "~" H 8850 6200 50  0001 C CNN
	1    8850 6200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5CBB21A0
P 8850 5900
F 0 "H2" H 8950 5946 50  0000 L CNN
F 1 "M3_Hole" H 8950 5855 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 8850 5900 50  0001 C CNN
F 3 "~" H 8850 5900 50  0001 C CNN
	1    8850 5900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5CBB249D
P 9350 5900
F 0 "H3" H 9450 5946 50  0000 L CNN
F 1 "M3_Hole" H 9450 5855 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 9350 5900 50  0001 C CNN
F 3 "~" H 9350 5900 50  0001 C CNN
	1    9350 5900
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 5CBB26A1
P 9350 6200
F 0 "H6" H 9450 6246 50  0000 L CNN
F 1 "M3_Hole" H 9450 6155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3" H 9350 6200 50  0001 C CNN
F 3 "~" H 9350 6200 50  0001 C CNN
	1    9350 6200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
