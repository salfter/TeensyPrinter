EESchema Schematic File Version 4
LIBS:TeensyPrinter-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 10 10
Title "Teensy Pololu Shield"
Date "2019-03-28"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 5100 3100 0    47   Input ~ 0
X_EN
Text GLabel 5100 3000 0    47   Input ~ 0
Y_EN
Text GLabel 5100 2900 0    47   Input ~ 0
Z_EN
Text GLabel 5100 2800 0    47   Input ~ 0
E0_EN
Text GLabel 5100 2700 0    47   Input ~ 0
E1_EN
Text GLabel 5100 2600 0    47   Input ~ 0
E2_EN
Text GLabel 5100 4750 0    47   Input ~ 0
HEATER_BED
Text GLabel 5100 4850 0    47   Input ~ 0
HEATER_E0
Text GLabel 5100 4950 0    47   Input ~ 0
HEATER_E1
Text GLabel 5100 5050 0    47   Input ~ 0
HEATER_E2
Text GLabel 5100 5150 0    50   Input ~ 0
SOL_0
Text GLabel 5100 5250 0    50   Input ~ 0
SERVO_0
Text GLabel 5100 5350 0    50   Input ~ 0
SERVO_1
Text GLabel 3900 4150 0    50   Input ~ 0
SUICIDE
$Comp
L Connector_Generic:Conn_01x02 J36
U 1 1 5CB809AC
P 2950 4150
F 0 "J36" H 2950 4250 50  0000 C CNN
F 1 "Conn_01x02" H 2868 4276 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2950 4150 50  0001 C CNN
F 3 "~" H 2950 4150 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "77311-818-02LF" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "609-4938-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    2950 4150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3150 4250 3350 4250
$Comp
L power:+3.3V #PWR0110
U 1 1 5CB8190E
P 3150 4150
F 0 "#PWR0110" H 3150 4000 50  0001 C CNN
F 1 "+3.3V" H 3165 4323 50  0000 C CNN
F 2 "" H 3150 4150 50  0001 C CNN
F 3 "" H 3150 4150 50  0001 C CNN
	1    3150 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R45
U 1 1 5CB81B16
P 3350 4400
F 0 "R45" H 3418 4446 50  0000 L CNN
F 1 "1k" H 3418 4355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3390 4390 50  0001 C CNN
F 3 "~" H 3350 4400 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT1K00" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT1K00CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3350 4400
	1    0    0    -1  
$EndComp
Connection ~ 3350 4250
Wire Wire Line
	3350 4250 3900 4250
$Comp
L power:GND #PWR0112
U 1 1 5CB823CE
P 3350 4550
F 0 "#PWR0112" H 3350 4300 50  0001 C CNN
F 1 "GND" H 3355 4377 50  0000 C CNN
F 2 "" H 3350 4550 50  0001 C CNN
F 3 "" H 3350 4550 50  0001 C CNN
	1    3350 4550
	1    0    0    -1  
$EndComp
Text Notes 2850 4250 2    50   ~ 0
NC E-Stop Switch\n(or use a jumper)
$Comp
L power:+3.3V #PWR0103
U 1 1 5CB84864
P 5600 2100
F 0 "#PWR0103" H 5600 1950 50  0001 C CNN
F 1 "+3.3V" H 5615 2273 50  0000 C CNN
F 2 "" H 5600 2100 50  0001 C CNN
F 3 "" H 5600 2100 50  0001 C CNN
	1    5600 2100
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0111
U 1 1 5CB852FF
P 5600 4450
F 0 "#PWR0111" H 5600 4300 50  0001 C CNN
F 1 "+3.3V" H 5615 4623 50  0000 C CNN
F 2 "" H 5600 4450 50  0001 C CNN
F 3 "" H 5600 4450 50  0001 C CNN
	1    5600 4450
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5CB859A4
P 5600 3700
F 0 "#PWR0106" H 5600 3450 50  0001 C CNN
F 1 "GND" H 5605 3527 50  0000 C CNN
F 2 "" H 5600 3700 50  0001 C CNN
F 3 "" H 5600 3700 50  0001 C CNN
	1    5600 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0114
U 1 1 5CB85DCF
P 5600 6050
F 0 "#PWR0114" H 5600 5800 50  0001 C CNN
F 1 "GND" H 5605 5877 50  0000 C CNN
F 2 "" H 5600 6050 50  0001 C CNN
F 3 "" H 5600 6050 50  0001 C CNN
	1    5600 6050
	1    0    0    -1  
$EndComp
Text GLabel 7050 3100 2    50   Output ~ 0
X_EN_B
Text GLabel 7050 3000 2    50   Output ~ 0
Y_EN_B
Text GLabel 7050 2900 2    50   Output ~ 0
Z_EN_B
Text GLabel 7050 2800 2    50   Output ~ 0
E0_EN_B
$Comp
L proper-passives:R_Pack08 RN1
U 1 1 5CB8DB42
P 6650 2000
F 0 "RN1" H 7050 2050 50  0000 L CNN
F 1 "10k" H 7050 1950 50  0000 L CNN
F 2 "passives:Panasonic_EXB-2HVxxxJV" V 7125 2000 50  0001 C CNN
F 3 "~" H 6650 2000 50  0001 C CNN
F 4 "Panasonic" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "EXB-2HV103JV" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "Y1103CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6650 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 2200 6450 2600
Wire Wire Line
	6450 2600 6100 2600
Wire Wire Line
	6550 2200 6550 2700
Wire Wire Line
	6550 2700 6100 2700
Wire Wire Line
	6650 2200 6650 2800
Wire Wire Line
	6650 2800 6100 2800
Wire Wire Line
	6750 2200 6750 2900
Wire Wire Line
	6750 2900 6100 2900
Wire Wire Line
	6850 2200 6850 3000
Wire Wire Line
	6850 3000 6100 3000
Wire Wire Line
	6950 2200 6950 3100
Wire Wire Line
	6950 3100 6100 3100
Wire Wire Line
	6450 2600 7050 2600
Connection ~ 6450 2600
Wire Wire Line
	6550 2700 7050 2700
Connection ~ 6550 2700
Wire Wire Line
	6650 2800 7050 2800
Connection ~ 6650 2800
Wire Wire Line
	6750 2900 7050 2900
Connection ~ 6750 2900
Wire Wire Line
	6850 3000 7050 3000
Connection ~ 6850 3000
Wire Wire Line
	6950 3100 7050 3100
Connection ~ 6950 3100
Text GLabel 7050 2700 2    50   Output ~ 0
E1_EN_B
Text GLabel 7050 2600 2    50   Output ~ 0
E2_EN_B
Text GLabel 7050 4750 2    50   Output ~ 0
HEATER_BED_B
Text GLabel 7050 4850 2    50   Output ~ 0
HEATER_E0_B
Wire Wire Line
	6750 1800 6750 1750
Wire Wire Line
	6750 1750 6650 1750
Wire Wire Line
	6650 1750 6650 1800
Wire Wire Line
	6650 1750 6550 1750
Wire Wire Line
	6550 1750 6550 1800
Connection ~ 6650 1750
Wire Wire Line
	6550 1750 6450 1750
Wire Wire Line
	6450 1750 6450 1800
Connection ~ 6550 1750
$Comp
L power:+3.3V #PWR0101
U 1 1 5CB97967
P 7100 1750
F 0 "#PWR0101" H 7100 1600 50  0001 C CNN
F 1 "+3.3V" H 7115 1923 50  0000 C CNN
F 2 "" H 7100 1750 50  0001 C CNN
F 3 "" H 7100 1750 50  0001 C CNN
	1    7100 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6850 1800 6850 1750
Wire Wire Line
	6850 1750 6950 1750
Wire Wire Line
	6950 1750 6950 1800
Wire Wire Line
	6950 1750 7100 1750
Connection ~ 6950 1750
Wire Wire Line
	5050 5450 5100 5450
$Comp
L proper-passives:R_Pack08 RN2
U 1 1 5CB9DF2D
P 6650 4350
F 0 "RN2" H 6050 4400 50  0000 L CNN
F 1 "10k" H 6050 4300 50  0000 L CNN
F 2 "passives:Panasonic_EXB-2HVxxxJV" V 7125 4350 50  0001 C CNN
F 3 "~" H 6650 4350 50  0001 C CNN
F 4 "Panasonic" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "EXB-2HV103JV" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "Y1103CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6650 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4150 6750 4100
Wire Wire Line
	6750 4100 6650 4100
Wire Wire Line
	6650 4100 6650 4150
Wire Wire Line
	6650 4100 6550 4100
Wire Wire Line
	6550 4100 6550 4150
Connection ~ 6650 4100
Wire Wire Line
	6550 4100 6450 4100
Wire Wire Line
	6450 4100 6450 4150
Connection ~ 6550 4100
Wire Wire Line
	6450 4100 6350 4100
Wire Wire Line
	6350 4100 6350 4150
Connection ~ 6450 4100
Wire Wire Line
	6350 4100 6250 4100
Wire Wire Line
	6250 4100 6250 4150
Connection ~ 6350 4100
Wire Wire Line
	6850 4150 6850 4100
Wire Wire Line
	6850 4100 6950 4100
Wire Wire Line
	6950 4100 6950 4150
Wire Wire Line
	6950 4100 7100 4100
Connection ~ 6950 4100
$Comp
L power:GND #PWR0109
U 1 1 5CBA9882
P 7100 4100
F 0 "#PWR0109" H 7100 3850 50  0001 C CNN
F 1 "GND" H 7105 3927 50  0000 C CNN
F 2 "" H 7100 4100 50  0001 C CNN
F 3 "" H 7100 4100 50  0001 C CNN
	1    7100 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4100 6850 4100
Connection ~ 6750 4100
Connection ~ 6850 4100
Text GLabel 7050 4950 2    50   Output ~ 0
HEATER_E1_B
Text GLabel 7050 5050 2    50   Output ~ 0
HEATER_E2_B
Text GLabel 7050 5150 2    50   Output ~ 0
SOL_0_B
Text GLabel 7050 5250 2    50   Output ~ 0
SERVO_0_B
Text GLabel 7050 5350 2    50   Output ~ 0
SERVO_1_B
$Comp
L Device:C C18
U 1 1 5CBBFE3F
P 8000 3600
F 0 "C18" H 8115 3646 50  0000 L CNN
F 1 "0.1u" H 8115 3555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8038 3450 50  0001 C CNN
F 3 "~" H 8000 3600 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "885012207098" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8080-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    8000 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C19
U 1 1 5CBC0559
P 8500 3600
F 0 "C19" H 8615 3646 50  0000 L CNN
F 1 "0.1u" H 8615 3555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8538 3450 50  0001 C CNN
F 3 "~" H 8500 3600 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "885012207098" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8080-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    8500 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5CBC0791
P 8500 3750
F 0 "#PWR0108" H 8500 3500 50  0001 C CNN
F 1 "GND" H 8505 3577 50  0000 C CNN
F 2 "" H 8500 3750 50  0001 C CNN
F 3 "" H 8500 3750 50  0001 C CNN
	1    8500 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5CBC0B0A
P 8000 3750
F 0 "#PWR0107" H 8000 3500 50  0001 C CNN
F 1 "GND" H 8005 3577 50  0000 C CNN
F 2 "" H 8000 3750 50  0001 C CNN
F 3 "" H 8000 3750 50  0001 C CNN
	1    8000 3750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0104
U 1 1 5CBC0D74
P 8000 3450
F 0 "#PWR0104" H 8000 3300 50  0001 C CNN
F 1 "+3.3V" H 8015 3623 50  0000 C CNN
F 2 "" H 8000 3450 50  0001 C CNN
F 3 "" H 8000 3450 50  0001 C CNN
	1    8000 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0105
U 1 1 5CBC14E8
P 8500 3450
F 0 "#PWR0105" H 8500 3300 50  0001 C CNN
F 1 "+3.3V" H 8515 3623 50  0000 C CNN
F 2 "" H 8500 3450 50  0001 C CNN
F 3 "" H 8500 3450 50  0001 C CNN
	1    8500 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C20
U 1 1 5CBC25C2
P 9000 3600
F 0 "C20" H 9115 3646 50  0000 L CNN
F 1 "0.1u" H 9115 3555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9038 3450 50  0001 C CNN
F 3 "~" H 9000 3600 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "885012207098" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8080-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    9000 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5CBC25CC
P 9000 3750
F 0 "#PWR0116" H 9000 3500 50  0001 C CNN
F 1 "GND" H 9005 3577 50  0000 C CNN
F 2 "" H 9000 3750 50  0001 C CNN
F 3 "" H 9000 3750 50  0001 C CNN
	1    9000 3750
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q?
U 1 1 5CBFB98F
P 3900 5750
AR Path="/5CAA15C8/5CBFB98F" Ref="Q?"  Part="1" 
AR Path="/5CB1DC3F/5CBFB98F" Ref="Q6"  Part="1" 
F 0 "Q6" H 4106 5796 50  0000 L CNN
F 1 "ZXMN3F30FH" H 4106 5705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4100 5850 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/ZXMN3F30FH.pdf" H 3900 5750 50  0001 C CNN
F 4 "Diodes Incorporated" H 3900 5750 50  0001 C CNN "DK_Mfr"
F 5 "ZXMN3F30FHTA" H 3900 5750 50  0001 C CNN "DK_Mfr_PN"
F 6 "ZXMN3F30FHCT-ND" H 3900 5750 50  0001 C CNN "DK_PN"
F 7 "SOT23" H 3900 5750 50  0001 C CNN "Package"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3900 5750
	-1   0    0    -1  
$EndComp
$Comp
L Device:R_US R47
U 1 1 5CC03ABB
P 4300 5750
F 0 "R47" V 4095 5750 50  0000 C CNN
F 1 "100" V 4186 5750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4340 5740 50  0001 C CNN
F 3 "~" H 4300 5750 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100R" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100RCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4300 5750
	0    1    1    0   
$EndComp
Connection ~ 4450 5750
$Comp
L Device:LED_ALT D7
U 1 1 5CC0568E
P 3800 5350
F 0 "D7" V 3839 5232 50  0000 R CNN
F 1 "E_STOP_LED" V 3748 5232 50  0001 R CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 3800 5350 50  0001 C CNN
F 3 "~" H 3800 5350 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "150080RS75000" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-4984-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3800 5350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_US R46
U 1 1 5CC06580
P 3800 5000
F 0 "R46" H 3732 4954 50  0000 R CNN
F 1 "1k" H 3732 5045 50  0000 R CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3840 4990 50  0001 C CNN
F 3 "~" H 3800 5000 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT1K00" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT1K00CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3800 5000
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0118
U 1 1 5CC06EF7
P 3800 4800
F 0 "#PWR0118" H 3800 4650 50  0001 C CNN
F 1 "+5V" H 3815 4973 50  0000 C CNN
F 2 "" H 3800 4800 50  0001 C CNN
F 3 "" H 3800 4800 50  0001 C CNN
	1    3800 4800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5CC075F0
P 3800 6000
F 0 "#PWR0119" H 3800 5750 50  0001 C CNN
F 1 "GND" H 3805 5827 50  0000 C CNN
F 2 "" H 3800 6000 50  0001 C CNN
F 3 "" H 3800 6000 50  0001 C CNN
	1    3800 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3800 6000 3800 5950
Wire Wire Line
	3800 5550 3800 5500
Wire Wire Line
	3800 5200 3800 5150
Wire Wire Line
	3800 4850 3800 4800
$Comp
L Device:R_US R48
U 1 1 5CC17782
P 4000 5950
F 0 "R48" V 3900 5950 50  0000 C CNN
F 1 "100k" V 4100 5950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4040 5940 50  0001 C CNN
F 3 "~" H 4000 5950 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4000 5950
	0    1    1    0   
$EndComp
Wire Wire Line
	3800 5950 3850 5950
Connection ~ 3800 5950
Wire Wire Line
	4150 5950 4150 5750
Wire Wire Line
	4150 5750 4100 5750
Connection ~ 4150 5750
$Comp
L power:VCC #PWR0117
U 1 1 5CC212B1
P 9000 3450
F 0 "#PWR0117" H 9000 3300 50  0001 C CNN
F 1 "VCC" H 9017 3623 50  0000 C CNN
F 2 "" H 9000 3450 50  0001 C CNN
F 3 "" H 9000 3450 50  0001 C CNN
	1    9000 3450
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74LVC1G00 U13
U 1 1 5CB65736
P 4200 4200
F 0 "U13" H 4175 4467 50  0000 C CNN
F 1 "74LVC1G00" H 4175 4376 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4200 4200 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 4200 4200 50  0001 C CNN
F 4 "Texas Instruments" H 200 450 50  0001 C CNN "DK_Mfr"
F 5 "SN74LVC1G00DBVRG4" H 200 450 50  0001 C CNN "DK_Mfr_PN"
F 6 "296-49912-1-ND" H 200 450 50  0001 C CNN "DK_PN"
F 7 "SOT-23-5" H 200 450 50  0001 C CNN "Package"
F 8 "any" H 200 450 50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4200 4200
	1    0    0    -1  
$EndComp
Connection ~ 4450 4200
Wire Wire Line
	4450 4200 4450 5750
$Comp
L 74xx:74LS245 U12
U 1 1 5CBEAAEB
P 5600 2900
F 0 "U12" H 5400 3550 50  0000 C CNN
F 1 "74LCX245" H 5850 2250 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 5600 2900 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/74LCX245-D.pdf" H 5600 2900 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "74LCX245MTCX" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "74LCX245MTCXCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "20-TSSOP" H 0   0   50  0001 C CNN "Package"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
	1    5600 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3400 4450 4200
Wire Wire Line
	4450 3400 5100 3400
$Comp
L power:+3.3V #PWR0113
U 1 1 5CBF1B74
P 4450 3300
F 0 "#PWR0113" H 4450 3150 50  0001 C CNN
F 1 "+3.3V" H 4465 3473 50  0000 C CNN
F 2 "" H 4450 3300 50  0001 C CNN
F 3 "" H 4450 3300 50  0001 C CNN
	1    4450 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3300 5100 3300
$Comp
L 74xx:74LS245 U14
U 1 1 5CBF53AF
P 5600 5250
F 0 "U14" H 5400 5900 50  0000 C CNN
F 1 "74LCX245" H 5850 4600 50  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 5600 5250 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/74LCX245-D.pdf" H 5600 5250 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "74LCX245MTCX" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "74LCX245MTCXCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "20-TSSOP" H 0   0   50  0001 C CNN "Package"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
F 9 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
	1    5600 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 5750 5100 5750
Wire Wire Line
	5050 5450 5050 6050
Wire Wire Line
	5050 6050 5600 6050
Connection ~ 5600 6050
$Comp
L power:+3.3V #PWR0121
U 1 1 5CBFC877
P 4850 5650
F 0 "#PWR0121" H 4850 5500 50  0001 C CNN
F 1 "+3.3V" H 4865 5823 50  0000 C CNN
F 2 "" H 4850 5650 50  0001 C CNN
F 3 "" H 4850 5650 50  0001 C CNN
	1    4850 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 5650 5100 5650
Wire Wire Line
	6750 1750 6850 1750
Connection ~ 6750 1750
Connection ~ 6850 1750
Wire Wire Line
	6100 5150 6650 5150
Wire Wire Line
	6100 5250 6750 5250
Wire Wire Line
	6100 5350 6850 5350
Wire Wire Line
	6100 4950 6450 4950
Wire Wire Line
	6100 5050 6550 5050
NoConn ~ 6100 5450
Wire Wire Line
	6100 4850 6350 4850
Wire Wire Line
	6100 4750 6250 4750
NoConn ~ 6950 4550
Wire Wire Line
	6250 4550 6250 4750
Connection ~ 6250 4750
Wire Wire Line
	6250 4750 7050 4750
Wire Wire Line
	6350 4550 6350 4850
Connection ~ 6350 4850
Wire Wire Line
	6350 4850 7050 4850
Wire Wire Line
	6450 4550 6450 4950
Connection ~ 6450 4950
Wire Wire Line
	6450 4950 7050 4950
Wire Wire Line
	6550 4550 6550 5050
Connection ~ 6550 5050
Wire Wire Line
	6550 5050 7050 5050
Wire Wire Line
	6650 4550 6650 5150
Connection ~ 6650 5150
Wire Wire Line
	6650 5150 7050 5150
Wire Wire Line
	6750 4550 6750 5250
Connection ~ 6750 5250
Wire Wire Line
	6750 5250 7050 5250
Wire Wire Line
	6850 4550 6850 5350
Connection ~ 6850 5350
Wire Wire Line
	6850 5350 7050 5350
Wire Wire Line
	5600 3700 4800 3700
Wire Wire Line
	4800 3700 4800 2500
Wire Wire Line
	4800 2500 5100 2500
Connection ~ 5600 3700
Wire Wire Line
	5100 2500 5100 2400
Connection ~ 5100 2500
NoConn ~ 6100 2400
NoConn ~ 6100 2500
NoConn ~ 6250 2200
NoConn ~ 6350 2200
Wire Wire Line
	6450 1750 6350 1750
Wire Wire Line
	6350 1750 6350 1800
Connection ~ 6450 1750
Wire Wire Line
	6250 1800 6250 1750
Wire Wire Line
	6250 1750 6350 1750
Connection ~ 6350 1750
$EndSCHEMATC
