EESchema Schematic File Version 4
LIBS:TeensyPrinter-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 4 10
Title "Teensy Pololu Shield"
Date "2019-03-28"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J13
U 1 1 5C7F5945
P 5600 3350
F 0 "J13" H 5650 3767 50  0000 C CNN
F 1 "DISP1" H 5650 3676 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 5600 3350 50  0001 C CNN
F 3 "~" H 5600 3350 50  0001 C CNN
F 4 "METZ CONNECT USA Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PR20205VBDN" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "1849-1002-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5600 3350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J14
U 1 1 5C7F59A8
P 5600 4150
F 0 "J14" H 5650 4567 50  0000 C CNN
F 1 "DISP2" H 5650 4476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 5600 4150 50  0001 C CNN
F 3 "~" H 5600 4150 50  0001 C CNN
F 4 "METZ CONNECT USA Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PR20205VBDN" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "1849-1002-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5600 4150
	1    0    0    -1  
$EndComp
Text GLabel 3850 2800 0    50   Input ~ 0
LCD_EN
Text GLabel 3850 3350 0    50   Input ~ 0
LCD_D4
Text GLabel 7450 2850 2    50   Input ~ 0
LCD_RS
Text GLabel 6000 3150 2    50   UnSpc ~ 0
BTN_ENC
$Comp
L power:+5V #PWR043
U 1 1 5C7F5A2C
P 5350 3100
F 0 "#PWR043" H 5350 2950 50  0001 C CNN
F 1 "+5V" H 5365 3273 50  0000 C CNN
F 2 "" H 5350 3100 50  0001 C CNN
F 3 "" H 5350 3100 50  0001 C CNN
	1    5350 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 3100 5350 3550
Wire Wire Line
	5350 3550 5400 3550
$Comp
L power:GND #PWR044
U 1 1 5C7F5A7D
P 5950 3600
F 0 "#PWR044" H 5950 3350 50  0001 C CNN
F 1 "GND" H 5955 3427 50  0000 C CNN
F 2 "" H 5950 3600 50  0001 C CNN
F 3 "" H 5950 3600 50  0001 C CNN
	1    5950 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 3550 5950 3550
Wire Wire Line
	5950 3550 5950 3600
Wire Wire Line
	5900 3150 6000 3150
Text GLabel 6000 4150 2    50   Output ~ 0
SD_SDI
Wire Wire Line
	5900 4150 6000 4150
Text GLabel 5300 3950 0    50   Input ~ 0
SD_SDO
Wire Wire Line
	5300 3950 5400 3950
Text GLabel 6000 3950 2    50   Output ~ 0
SD_SCK
Wire Wire Line
	6000 3950 5900 3950
Text GLabel 6000 4050 2    50   Output ~ 0
SD_CS
Wire Wire Line
	5900 4050 6000 4050
Text GLabel 5300 4050 0    50   UnSpc ~ 0
BTN_EN2
Text GLabel 5300 4150 0    50   UnSpc ~ 0
BTN_EN1
Text GLabel 5300 4250 0    50   Input ~ 0
SD_DET
NoConn ~ 5900 4350
$Comp
L power:GND #PWR045
U 1 1 5C7F9F42
P 5350 4400
F 0 "#PWR045" H 5350 4150 50  0001 C CNN
F 1 "GND" H 5355 4227 50  0000 C CNN
F 2 "" H 5350 4400 50  0001 C CNN
F 3 "" H 5350 4400 50  0001 C CNN
	1    5350 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 4050 5400 4050
Wire Wire Line
	5400 4150 5300 4150
Wire Wire Line
	5300 4250 5400 4250
Wire Wire Line
	5400 4350 5350 4350
Wire Wire Line
	5350 4350 5350 4400
Text GLabel 7450 3350 2    50   Input ~ 0
LCD_D5
Text GLabel 7450 3850 2    50   Input ~ 0
LCD_D7
Text GLabel 3850 3900 0    50   Input ~ 0
LCD_D6
$Comp
L 74xx:74LCX07 U8
U 1 1 5C98983C
P 4150 2800
F 0 "U8" H 4150 3117 50  0000 C CNN
F 1 "74LCX07" H 4150 3026 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4150 2800 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 4150 2800 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4150 2800
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U8
U 2 1 5C98A261
P 4150 3350
F 0 "U8" H 4150 3667 50  0000 C CNN
F 1 "74LCX07" H 4150 3576 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4150 3350 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 4150 3350 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	2    4150 3350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U8
U 3 1 5C98AC9C
P 4150 3900
F 0 "U8" H 4150 4217 50  0000 C CNN
F 1 "74LCX07" H 4150 4126 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4150 3900 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 4150 3900 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	3    4150 3900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U8
U 4 1 5C98B78F
P 7150 2850
F 0 "U8" H 7150 3167 50  0000 C CNN
F 1 "74LCX07" H 7150 3076 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 7150 2850 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 7150 2850 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	4    7150 2850
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74LCX07 U8
U 5 1 5C98C2D2
P 7150 3350
F 0 "U8" H 7150 3667 50  0000 C CNN
F 1 "74LCX07" H 7150 3576 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 7150 3350 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 7150 3350 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	5    7150 3350
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74LCX07 U8
U 6 1 5C98C8F1
P 7150 3850
F 0 "U8" H 7150 4167 50  0000 C CNN
F 1 "74LCX07" H 7150 4076 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 7150 3850 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 7150 3850 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	6    7150 3850
	-1   0    0    1   
$EndComp
$Comp
L 74xx:74LCX07 U8
U 7 1 5C98CD16
P 5550 5900
F 0 "U8" H 5780 5946 50  0000 L CNN
F 1 "74LCX07" H 5780 5855 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 5550 5900 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 5550 5900 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	7    5550 5900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR051
U 1 1 5C98EE0A
P 5550 5400
F 0 "#PWR051" H 5550 5250 50  0001 C CNN
F 1 "+5V" H 5565 5573 50  0000 C CNN
F 2 "" H 5550 5400 50  0001 C CNN
F 3 "" H 5550 5400 50  0001 C CNN
	1    5550 5400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR052
U 1 1 5C98F75C
P 5550 6400
F 0 "#PWR052" H 5550 6150 50  0001 C CNN
F 1 "GND" H 5555 6227 50  0000 C CNN
F 2 "" H 5550 6400 50  0001 C CNN
F 3 "" H 5550 6400 50  0001 C CNN
	1    5550 6400
	1    0    0    -1  
$EndComp
Text GLabel 5300 3150 0    50   Input ~ 0
BEEPER
Wire Wire Line
	6600 3250 6600 2850
Wire Wire Line
	5900 3250 6600 3250
Wire Wire Line
	5300 3150 5400 3150
Wire Wire Line
	4800 2800 4800 3250
Wire Wire Line
	4800 3250 5400 3250
Wire Wire Line
	4800 3900 4800 3450
Wire Wire Line
	4800 3450 5400 3450
Wire Wire Line
	6600 3850 6600 3450
Wire Wire Line
	5900 3450 6600 3450
NoConn ~ 5900 4250
$Comp
L Device:C C7
U 1 1 5C9A39FA
P 5050 5900
F 0 "C7" H 5165 5946 50  0000 L CNN
F 1 "0.1u" H 5165 5855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5088 5750 50  0001 C CNN
F 3 "~" H 5050 5900 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "885012207098" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8080-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5050 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 5750 5050 5400
Wire Wire Line
	5050 5400 5550 5400
Connection ~ 5550 5400
Wire Wire Line
	5050 6050 5050 6400
Wire Wire Line
	5050 6400 5550 6400
Connection ~ 5550 6400
$Comp
L proper-passives:R_Pack08 RN?
U 1 1 5C9CF9AD
P 5750 1800
AR Path="/5CB1DC3F/5C9CF9AD" Ref="RN?"  Part="1" 
AR Path="/5C9CF9AD" Ref="RN?"  Part="1" 
AR Path="/5C7EEDF9/5C9CF9AD" Ref="RN4"  Part="1" 
F 0 "RN4" H 5150 1850 50  0000 L CNN
F 1 "10k" H 5150 1750 50  0000 L CNN
F 2 "passives:Panasonic_EXB-2HVxxxJV" V 6225 1800 50  0001 C CNN
F 3 "~" H 5750 1800 50  0001 C CNN
F 4 "Panasonic" H 3850 400 50  0001 C CNN "DK_Mfr"
F 5 "EXB-2HV103JV" H 3850 400 50  0001 C CNN "DK_Mfr_PN"
F 6 "Y1103CT-ND" H 3850 400 50  0001 C CNN "DK_PN"
F 7 "1" H 3850 400 50  0001 C CNN "Qty Per Unit"
	1    5750 1800
	1    0    0    1   
$EndComp
$Comp
L power:+5V #PWR049
U 1 1 5C9D06D7
P 5350 1600
F 0 "#PWR049" H 5350 1450 50  0001 C CNN
F 1 "+5V" H 5365 1773 50  0000 C CNN
F 2 "" H 5350 1600 50  0001 C CNN
F 3 "" H 5350 1600 50  0001 C CNN
	1    5350 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 1600 5450 1600
Wire Wire Line
	5550 1600 5450 1600
Connection ~ 5450 1600
Wire Wire Line
	5650 1600 5550 1600
Connection ~ 5550 1600
Wire Wire Line
	5750 1600 5650 1600
Connection ~ 5650 1600
Wire Wire Line
	5850 1600 5750 1600
Connection ~ 5750 1600
Wire Wire Line
	5950 1600 5850 1600
Connection ~ 5850 1600
Wire Wire Line
	6050 1600 5950 1600
Connection ~ 5950 1600
Wire Wire Line
	4450 2800 4650 2800
Wire Wire Line
	4450 3350 4700 3350
Wire Wire Line
	4450 3900 4750 3900
Wire Wire Line
	5350 2000 5350 2300
Wire Wire Line
	5350 2300 4650 2300
Wire Wire Line
	4650 2300 4650 2800
Connection ~ 4650 2800
Wire Wire Line
	4650 2800 4800 2800
Connection ~ 4700 3350
Wire Wire Line
	4700 3350 5400 3350
Wire Wire Line
	5550 2500 4750 2500
Wire Wire Line
	4750 2500 4750 3900
Connection ~ 4750 3900
Wire Wire Line
	4750 3900 4800 3900
Wire Wire Line
	6600 2850 6750 2850
Wire Wire Line
	5900 3350 6700 3350
Wire Wire Line
	6600 3850 6650 3850
Wire Wire Line
	5650 2500 6650 2500
Wire Wire Line
	6650 2500 6650 3850
Connection ~ 6650 3850
Wire Wire Line
	6650 3850 6850 3850
Wire Wire Line
	5750 2400 6700 2400
Wire Wire Line
	6700 2400 6700 3350
Connection ~ 6700 3350
Wire Wire Line
	6700 3350 6850 3350
Wire Wire Line
	5850 2300 6750 2300
Wire Wire Line
	6750 2300 6750 2850
Connection ~ 6750 2850
Wire Wire Line
	6750 2850 6850 2850
Connection ~ 5350 1600
Wire Wire Line
	5650 2500 5650 2300
Wire Wire Line
	5650 2300 5450 2300
Wire Wire Line
	5450 2300 5450 2000
Wire Wire Line
	4700 2600 5700 2600
Wire Wire Line
	5700 2600 5700 2150
Wire Wire Line
	4700 2600 4700 3350
Wire Wire Line
	5550 2150 5550 2000
Wire Wire Line
	5550 2150 5700 2150
Wire Wire Line
	5750 2400 5750 2100
Wire Wire Line
	5750 2100 5650 2100
Wire Wire Line
	5650 2100 5650 2000
Wire Wire Line
	5550 2500 5550 2550
Wire Wire Line
	5550 2550 5800 2550
Wire Wire Line
	5800 2550 5800 2050
Wire Wire Line
	5800 2050 5750 2050
Wire Wire Line
	5750 2050 5750 2000
Wire Wire Line
	5850 2300 5850 2000
$EndSCHEMATC
