EESchema Schematic File Version 4
LIBS:TeensyPrinter-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 2 10
Title "Teensy Pololu Shield"
Date "2019-03-28"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pololu:POLOLU U2
U 1 1 5C876E8F
P 3850 1700
F 0 "U2" H 3850 2447 60  0000 C CNN
F 1 "X" H 3850 2341 60  0000 C CNN
F 2 "pololu:pololu" H 3850 1700 60  0001 C CNN
F 3 "" H 3850 1700 60  0000 C CNN
F 4 "Samtec Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "SSA-117-S-T" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM1122-17-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3850 1700
	1    0    0    -1  
$EndComp
Text GLabel 2250 1500 0    50   Input ~ 0
X_STEP
Text GLabel 2250 1600 0    50   Input ~ 0
X_DIR
Text GLabel 2250 1300 0    50   Input ~ 0
X_EN_B
$Comp
L Device:R_US R1
U 1 1 5C876F59
P 2900 1150
F 0 "R1" H 2750 1200 50  0000 L CNN
F 1 "10k" H 2700 1100 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2940 1140 50  0001 C CNN
F 3 "~" H 2900 1150 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT10K0" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT10K0CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    2900 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 1300 3050 1300
Connection ~ 2900 1300
Wire Wire Line
	2900 950  2900 1000
Wire Wire Line
	3050 1150 3050 1200
$Comp
L power:+12V #PWR03
U 1 1 5C87712B
P 4650 1150
F 0 "#PWR03" H 4650 1000 50  0001 C CNN
F 1 "+12V" H 4665 1323 50  0000 C CNN
F 2 "" H 4650 1150 50  0001 C CNN
F 3 "" H 4650 1150 50  0001 C CNN
	1    4650 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 1150 4650 1200
$Comp
L conns:Conn_Generic_3x04 J2
U 1 1 5C88231C
P 2550 2100
F 0 "J2" H 2650 2350 50  0000 C CNN
F 1 "Conn_Generic_3x04" H 2550 2487 50  0001 C CNN
F 2 "conns:PinHeader_3x04_P2.54mm_Vertical" H 2550 1700 50  0001 C CNN
F 3 "" H 2550 1700 50  0001 C CNN
F 4 "Samtec" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "TSW-104-07-L-T" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM12310-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    2550 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 1800 3050 1800
Wire Wire Line
	2500 1800 2500 1700
Wire Wire Line
	2500 1700 2950 1700
Wire Wire Line
	2500 2400 2500 2450
Wire Wire Line
	2500 2450 2850 2450
Wire Wire Line
	2850 2450 2850 1900
Wire Wire Line
	2850 1900 3050 1900
Wire Wire Line
	2600 2400 2900 2400
Wire Wire Line
	2900 2400 2900 2100
Wire Wire Line
	2900 2100 3050 2100
Wire Wire Line
	3050 2200 3000 2200
Wire Wire Line
	3000 2200 3000 2150
Wire Wire Line
	3000 1200 3050 1200
Connection ~ 3050 1200
Wire Wire Line
	2800 1950 2800 2050
Wire Wire Line
	2800 2050 2800 2150
Connection ~ 2800 2050
Wire Wire Line
	2800 2150 2800 2250
Connection ~ 2800 2150
Wire Wire Line
	2800 2150 3000 2150
Connection ~ 3000 2150
Wire Wire Line
	3000 2150 3000 1200
Text GLabel 2250 1950 0    50   Input ~ 0
TMC_SDI
Text GLabel 2250 2050 0    50   Input ~ 0
TMC_SCK
Text GLabel 2250 2150 0    50   Input ~ 0
X_CS
Text GLabel 1400 2150 2    50   Output ~ 0
TMC_SDO
Wire Wire Line
	2250 1950 2300 1950
Wire Wire Line
	2250 2050 2300 2050
Wire Wire Line
	2250 2150 2300 2150
$Comp
L Device:R_US R2
U 1 1 5C883E2C
P 2950 2350
F 0 "R2" H 3000 2400 50  0000 L CNN
F 1 "100k" H 3000 2300 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2990 2340 50  0001 C CNN
F 3 "~" H 2950 2350 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    2950 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2200 2950 1700
Connection ~ 2950 1700
Wire Wire Line
	2950 1700 3050 1700
$Comp
L power:GND #PWR05
U 1 1 5C8842D6
P 2950 2550
F 0 "#PWR05" H 2950 2300 50  0001 C CNN
F 1 "GND" H 2955 2377 50  0000 C CNN
F 2 "" H 2950 2550 50  0001 C CNN
F 3 "" H 2950 2550 50  0001 C CNN
	1    2950 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 2550 2950 2500
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5C884790
P 4900 1400
F 0 "J1" H 4980 1392 50  0000 L CNN
F 1 "X_MOT" H 4980 1301 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4900 1400 50  0001 C CNN
F 3 "~" H 4900 1400 50  0001 C CNN
F 4 "Sullins" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PRPC004SAAN-RC" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-04-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4900 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 1300 4700 1300
Wire Wire Line
	4650 1400 4700 1400
Wire Wire Line
	4650 1500 4700 1500
Wire Wire Line
	4650 1600 4700 1600
Text GLabel 5050 1900 2    50   Output ~ 0
X_MIN
$Comp
L power:GND #PWR06
U 1 1 5C88614B
P 4650 2550
F 0 "#PWR06" H 4650 2300 50  0001 C CNN
F 1 "GND" H 4655 2377 50  0000 C CNN
F 2 "" H 4650 2550 50  0001 C CNN
F 3 "" H 4650 2550 50  0001 C CNN
	1    4650 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2100 4650 2200
Wire Wire Line
	4650 2200 4650 2550
Connection ~ 4650 2200
Wire Wire Line
	2250 1300 2900 1300
Wire Wire Line
	2250 1500 3050 1500
Wire Wire Line
	2250 1600 3050 1600
$Comp
L pololu:POLOLU U3
U 1 1 5C888433
P 3900 4000
F 0 "U3" H 3900 4747 60  0000 C CNN
F 1 "Y" H 3900 4641 60  0000 C CNN
F 2 "pololu:pololu" H 3900 4000 60  0001 C CNN
F 3 "" H 3900 4000 60  0000 C CNN
F 4 "Samtec Inc." H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "SSA-117-S-T" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM1122-17-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    3900 4000
	1    0    0    -1  
$EndComp
Text GLabel 2300 3800 0    50   Input ~ 0
Y_STEP
Text GLabel 2300 3900 0    50   Input ~ 0
Y_DIR
Text GLabel 2300 3600 0    50   Input ~ 0
Y_EN_B
$Comp
L Device:R_US R3
U 1 1 5C88843D
P 2950 3450
F 0 "R3" H 2800 3500 50  0000 L CNN
F 1 "10k" H 2750 3400 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2990 3440 50  0001 C CNN
F 3 "~" H 2950 3450 50  0001 C CNN
F 4 "Stackpole " H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT10K0" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT10K0CT-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "0805" H 50  0   50  0001 C CNN "Package"
F 8 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    2950 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 3600 3100 3600
Connection ~ 2950 3600
Wire Wire Line
	2950 3250 2950 3300
Wire Wire Line
	3100 3450 3100 3500
$Comp
L power:+12V #PWR010
U 1 1 5C888454
P 4700 3450
F 0 "#PWR010" H 4700 3300 50  0001 C CNN
F 1 "+12V" H 4715 3623 50  0000 C CNN
F 2 "" H 4700 3450 50  0001 C CNN
F 3 "" H 4700 3450 50  0001 C CNN
	1    4700 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3450 4700 3500
$Comp
L conns:Conn_Generic_3x04 J4
U 1 1 5C88845B
P 2600 4400
F 0 "J4" H 2700 4650 50  0000 C CNN
F 1 "Conn_Generic_3x04" H 2600 4787 50  0001 C CNN
F 2 "conns:PinHeader_3x04_P2.54mm_Vertical" H 2600 4000 50  0001 C CNN
F 3 "" H 2600 4000 50  0001 C CNN
F 4 "Samtec" H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "TSW-104-07-L-T" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM12310-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    2600 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 4100 3100 4100
Wire Wire Line
	2550 4100 2550 4000
Wire Wire Line
	2550 4000 3000 4000
Wire Wire Line
	2550 4700 2550 4750
Wire Wire Line
	2550 4750 2900 4750
Wire Wire Line
	2900 4750 2900 4200
Wire Wire Line
	2900 4200 3100 4200
Wire Wire Line
	2650 4700 2950 4700
Wire Wire Line
	2950 4700 2950 4400
Wire Wire Line
	2950 4400 3100 4400
Wire Wire Line
	3100 4500 3050 4500
Wire Wire Line
	3050 4500 3050 4450
Wire Wire Line
	3050 3500 3100 3500
Connection ~ 3100 3500
Wire Wire Line
	2850 4250 2850 4350
Wire Wire Line
	2850 4350 2850 4450
Connection ~ 2850 4350
Wire Wire Line
	2850 4450 2850 4550
Connection ~ 2850 4450
Wire Wire Line
	2850 4450 3050 4450
Connection ~ 3050 4450
Wire Wire Line
	3050 4450 3050 3500
Text GLabel 1400 4350 2    50   Input ~ 0
TMC_SDI
Text GLabel 2300 4350 0    50   Input ~ 0
TMC_SCK
Text GLabel 2300 4450 0    50   Input ~ 0
Y_CS
Wire Wire Line
	2300 4350 2350 4350
Wire Wire Line
	2300 4450 2350 4450
$Comp
L Device:R_US R4
U 1 1 5C888480
P 3000 4650
F 0 "R4" H 3050 4700 50  0000 L CNN
F 1 "100k" H 3050 4600 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3040 4640 50  0001 C CNN
F 3 "~" H 3000 4650 50  0001 C CNN
F 4 "Stackpole " H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "0805" H 50  0   50  0001 C CNN "Package"
F 8 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    3000 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4500 3000 4000
Connection ~ 3000 4000
Wire Wire Line
	3000 4000 3100 4000
$Comp
L power:GND #PWR012
U 1 1 5C88848A
P 3000 4850
F 0 "#PWR012" H 3000 4600 50  0001 C CNN
F 1 "GND" H 3005 4677 50  0000 C CNN
F 2 "" H 3000 4850 50  0001 C CNN
F 3 "" H 3000 4850 50  0001 C CNN
	1    3000 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 4850 3000 4800
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 5C888491
P 4950 3700
F 0 "J3" H 5030 3692 50  0000 L CNN
F 1 "Y_MOT" H 5030 3601 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4950 3700 50  0001 C CNN
F 3 "~" H 4950 3700 50  0001 C CNN
F 4 "Sullins" H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "PRPC004SAAN-RC" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-04-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    4950 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 3600 4750 3600
Wire Wire Line
	4700 3700 4750 3700
Wire Wire Line
	4700 3800 4750 3800
Wire Wire Line
	4700 3900 4750 3900
Text GLabel 5100 4200 2    50   Output ~ 0
Y_MIN
$Comp
L power:GND #PWR013
U 1 1 5C88849E
P 4700 4850
F 0 "#PWR013" H 4700 4600 50  0001 C CNN
F 1 "GND" H 4705 4677 50  0000 C CNN
F 2 "" H 4700 4850 50  0001 C CNN
F 3 "" H 4700 4850 50  0001 C CNN
	1    4700 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 4400 4700 4500
Wire Wire Line
	4700 4500 4700 4850
Wire Wire Line
	2300 3600 2950 3600
Wire Wire Line
	2300 3800 3100 3800
Wire Wire Line
	2300 3900 3100 3900
$Comp
L pololu:POLOLU U4
U 1 1 5C88D8E1
P 3900 6250
F 0 "U4" H 3900 6997 60  0000 C CNN
F 1 "Z" H 3900 6891 60  0000 C CNN
F 2 "pololu:pololu" H 3900 6250 60  0001 C CNN
F 3 "" H 3900 6250 60  0000 C CNN
F 4 "Samtec Inc." H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "SSA-117-S-T" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM1122-17-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    3900 6250
	1    0    0    -1  
$EndComp
Text GLabel 2300 6050 0    50   Input ~ 0
Z_STEP
Text GLabel 2300 6150 0    50   Input ~ 0
Z_DIR
Text GLabel 2300 5850 0    50   Input ~ 0
Z_EN_B
$Comp
L Device:R_US R5
U 1 1 5C88D8EB
P 2950 5700
F 0 "R5" H 2800 5750 50  0000 L CNN
F 1 "10k" H 2750 5650 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2990 5690 50  0001 C CNN
F 3 "~" H 2950 5700 50  0001 C CNN
F 4 "Stackpole " H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT10K0" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT10K0CT-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "0805" H 50  0   50  0001 C CNN "Package"
F 8 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    2950 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2950 5850 3100 5850
Connection ~ 2950 5850
Wire Wire Line
	2950 5500 2950 5550
Wire Wire Line
	3100 5700 3100 5750
$Comp
L power:+12V #PWR017
U 1 1 5C88D902
P 4700 5700
F 0 "#PWR017" H 4700 5550 50  0001 C CNN
F 1 "+12V" H 4715 5873 50  0000 C CNN
F 2 "" H 4700 5700 50  0001 C CNN
F 3 "" H 4700 5700 50  0001 C CNN
	1    4700 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 5700 4700 5750
$Comp
L conns:Conn_Generic_3x04 J6
U 1 1 5C88D909
P 2600 6650
F 0 "J6" H 2700 6900 50  0000 C CNN
F 1 "Conn_Generic_3x04" H 2600 7037 50  0001 C CNN
F 2 "conns:PinHeader_3x04_P2.54mm_Vertical" H 2600 6250 50  0001 C CNN
F 3 "" H 2600 6250 50  0001 C CNN
F 4 "Samtec" H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "TSW-104-07-L-T" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "SAM12310-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    2600 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 6350 3100 6350
Wire Wire Line
	2550 6350 2550 6250
Wire Wire Line
	2550 6250 3000 6250
Wire Wire Line
	2550 6950 2550 7000
Wire Wire Line
	2550 7000 2900 7000
Wire Wire Line
	2900 7000 2900 6450
Wire Wire Line
	2900 6450 3100 6450
Wire Wire Line
	2650 6950 2950 6950
Wire Wire Line
	2950 6950 2950 6650
Wire Wire Line
	2950 6650 3100 6650
Wire Wire Line
	3100 6750 3050 6750
Wire Wire Line
	3050 6750 3050 6700
Wire Wire Line
	3050 5750 3100 5750
Connection ~ 3100 5750
Wire Wire Line
	2850 6500 2850 6600
Wire Wire Line
	2850 6600 2850 6700
Connection ~ 2850 6600
Wire Wire Line
	2850 6700 2850 6800
Connection ~ 2850 6700
Wire Wire Line
	2850 6700 3050 6700
Connection ~ 3050 6700
Wire Wire Line
	3050 6700 3050 5750
Text GLabel 2300 6600 0    50   Input ~ 0
TMC_SCK
Text GLabel 2300 6700 0    50   Input ~ 0
Z_CS
Wire Wire Line
	2300 6600 2350 6600
Wire Wire Line
	2300 6700 2350 6700
$Comp
L Device:R_US R6
U 1 1 5C88D92E
P 3000 6900
F 0 "R6" H 3050 6950 50  0000 L CNN
F 1 "100k" H 3050 6850 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3040 6890 50  0001 C CNN
F 3 "~" H 3000 6900 50  0001 C CNN
F 4 "Stackpole " H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "0805" H 50  0   50  0001 C CNN "Package"
F 8 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    3000 6900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 6750 3000 6250
Connection ~ 3000 6250
Wire Wire Line
	3000 6250 3100 6250
$Comp
L power:GND #PWR019
U 1 1 5C88D938
P 3000 7100
F 0 "#PWR019" H 3000 6850 50  0001 C CNN
F 1 "GND" H 3005 6927 50  0000 C CNN
F 2 "" H 3000 7100 50  0001 C CNN
F 3 "" H 3000 7100 50  0001 C CNN
	1    3000 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 7100 3000 7050
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 5C88D93F
P 5300 5950
F 0 "J5" H 5380 5942 50  0000 L CNN
F 1 "P" H 5380 5851 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5300 5950 50  0001 C CNN
F 3 "~" H 5300 5950 50  0001 C CNN
F 4 "Sullins" H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "PRPC004SAAN-RC" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-04-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    5300 5950
	1    0    0    -1  
$EndComp
Text GLabel 5100 6450 2    50   Output ~ 0
Z_MIN
$Comp
L power:GND #PWR020
U 1 1 5C88D94C
P 4700 7100
F 0 "#PWR020" H 4700 6850 50  0001 C CNN
F 1 "GND" H 4705 6927 50  0000 C CNN
F 2 "" H 4700 7100 50  0001 C CNN
F 3 "" H 4700 7100 50  0001 C CNN
	1    4700 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 6650 4700 6750
Wire Wire Line
	4700 6750 4700 7100
Connection ~ 4700 6750
Wire Wire Line
	2300 5850 2950 5850
Wire Wire Line
	2300 6050 3100 6050
Wire Wire Line
	2300 6150 3100 6150
$Comp
L Device:CP1 C1
U 1 1 5C8B724A
P 3850 2700
F 0 "C1" H 3965 2746 50  0000 L CNN
F 1 "100uF 35V" H 3965 2655 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 3850 2700 50  0001 C CNN
F 3 "~" H 3850 2700 50  0001 C CNN
F 4 "Kemet" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "EDK107M035A9HAA" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "399-6671-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "6.3x7.7" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3850 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5C8B730C
P 3850 2900
F 0 "#PWR07" H 3850 2650 50  0001 C CNN
F 1 "GND" H 3855 2727 50  0000 C CNN
F 2 "" H 3850 2900 50  0001 C CNN
F 3 "" H 3850 2900 50  0001 C CNN
	1    3850 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 2500 3850 2550
Wire Wire Line
	3850 2850 3850 2900
$Comp
L Device:CP1 C2
U 1 1 5C8C1DDF
P 3900 5000
F 0 "C2" H 4015 5046 50  0000 L CNN
F 1 "100uF 35V" H 4015 4955 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 3900 5000 50  0001 C CNN
F 3 "~" H 3900 5000 50  0001 C CNN
F 4 "Kemet" H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "EDK107M035A9HAA" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "399-6671-1-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "6.3x7.7" H 50  0   50  0001 C CNN "Package"
F 8 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    3900 5000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5C8C1DE6
P 3900 5200
F 0 "#PWR014" H 3900 4950 50  0001 C CNN
F 1 "GND" H 3905 5027 50  0000 C CNN
F 2 "" H 3900 5200 50  0001 C CNN
F 3 "" H 3900 5200 50  0001 C CNN
	1    3900 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 4800 3900 4850
Wire Wire Line
	3900 5150 3900 5200
$Comp
L Device:CP1 C3
U 1 1 5C8C59B2
P 3900 7250
F 0 "C3" H 4015 7296 50  0000 L CNN
F 1 "100uF 35V" H 4015 7205 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x7.7" H 3900 7250 50  0001 C CNN
F 3 "~" H 3900 7250 50  0001 C CNN
F 4 "Kemet" H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "EDK107M035A9HAA" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "399-6671-1-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "6.3x7.7" H 50  0   50  0001 C CNN "Package"
F 8 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    3900 7250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5C8C59B9
P 3900 7450
F 0 "#PWR021" H 3900 7200 50  0001 C CNN
F 1 "GND" H 3905 7277 50  0000 C CNN
F 2 "" H 3900 7450 50  0001 C CNN
F 3 "" H 3900 7450 50  0001 C CNN
	1    3900 7450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3900 7050 3900 7100
Wire Wire Line
	3900 7400 3900 7450
$Comp
L power:+12V #PWR04
U 1 1 5C905D2D
P 3850 2500
F 0 "#PWR04" H 3850 2350 50  0001 C CNN
F 1 "+12V" H 3865 2673 50  0000 C CNN
F 2 "" H 3850 2500 50  0001 C CNN
F 3 "" H 3850 2500 50  0001 C CNN
	1    3850 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR011
U 1 1 5C906457
P 3900 4800
F 0 "#PWR011" H 3900 4650 50  0001 C CNN
F 1 "+12V" H 3915 4973 50  0000 C CNN
F 2 "" H 3900 4800 50  0001 C CNN
F 3 "" H 3900 4800 50  0001 C CNN
	1    3900 4800
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR018
U 1 1 5C9069E7
P 3900 7050
F 0 "#PWR018" H 3900 6900 50  0001 C CNN
F 1 "+12V" H 3915 7223 50  0000 C CNN
F 2 "" H 3900 7050 50  0001 C CNN
F 3 "" H 3900 7050 50  0001 C CNN
	1    3900 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J15
U 1 1 5C966390
P 4950 2100
F 0 "J15" V 4868 2180 50  0000 L CNN
F 1 "Conn_01x02" V 4913 2180 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 4950 2100 50  0001 C CNN
F 3 "~" H 4950 2100 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "77311-818-02LF" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "609-4938-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4950 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	4650 1900 4850 1900
Wire Wire Line
	4950 1900 5050 1900
$Comp
L Connector_Generic:Conn_01x02 J16
U 1 1 5C970097
P 5000 4400
F 0 "J16" V 4918 4480 50  0000 L CNN
F 1 "Conn_01x02" V 4963 4480 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5000 4400 50  0001 C CNN
F 3 "~" H 5000 4400 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "77311-818-02LF" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "609-4938-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    5000 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 4200 4900 4200
$Comp
L Connector_Generic:Conn_01x02 J19
U 1 1 5C975FBE
P 5000 6650
F 0 "J19" V 4918 6730 50  0000 L CNN
F 1 "Conn_01x02" V 4963 6730 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5000 6650 50  0001 C CNN
F 3 "~" H 5000 6650 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "77311-818-02LF" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "609-4938-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    5000 6650
	0    1    1    0   
$EndComp
Wire Wire Line
	4700 6450 4900 6450
Wire Wire Line
	5000 6450 5100 6450
$Comp
L Connector_Generic:Conn_01x04 J18
U 1 1 5CAF95D9
P 5300 5500
F 0 "J18" H 5380 5492 50  0000 L CNN
F 1 "P/S" H 5380 5401 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5300 5500 50  0001 C CNN
F 3 "~" H 5300 5500 50  0001 C CNN
F 4 "Sullins" H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "PRPC004SAAN-RC" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-04-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    5300 5500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J17
U 1 1 5CB224FA
P 5300 5050
F 0 "J17" H 5380 5042 50  0000 L CNN
F 1 "S" H 5380 4951 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5300 5050 50  0001 C CNN
F 3 "~" H 5300 5050 50  0001 C CNN
F 4 "Sullins" H 50  0   50  0001 C CNN "DK_Mfr"
F 5 "PRPC004SAAN-RC" H 50  0   50  0001 C CNN "DK_Mfr_PN"
F 6 "S1011EC-04-ND" H 50  0   50  0001 C CNN "DK_PN"
F 7 "any" H 50  0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 50  0   50  0001 C CNN "Qty Per Unit"
	1    5300 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4700 6150 5050 6150
Wire Wire Line
	4700 6050 5000 6050
Wire Wire Line
	4700 5950 4950 5950
Wire Wire Line
	4700 5850 4900 5850
Wire Wire Line
	5100 5700 5050 5700
Wire Wire Line
	5050 5700 5050 6150
Connection ~ 5050 6150
Wire Wire Line
	5050 6150 5100 6150
Wire Wire Line
	5100 5600 5050 5600
Wire Wire Line
	5050 5600 5050 5250
Wire Wire Line
	5050 5250 5100 5250
Wire Wire Line
	5100 5150 5000 5150
Wire Wire Line
	5000 5150 5000 6050
Connection ~ 5000 6050
Wire Wire Line
	5000 6050 5100 6050
Wire Wire Line
	4950 5950 4950 5500
Wire Wire Line
	4950 5500 5100 5500
Connection ~ 4950 5950
Wire Wire Line
	4950 5950 5100 5950
Wire Wire Line
	5100 5400 4950 5400
Wire Wire Line
	4950 5400 4950 5050
Wire Wire Line
	4950 5050 5100 5050
Wire Wire Line
	5100 4950 4900 4950
Wire Wire Line
	4900 4950 4900 5850
Connection ~ 4900 5850
Wire Wire Line
	4900 5850 5100 5850
Text Notes 5850 5800 0    50   ~ 0
Z-axis motor connection notes:\n\n* one motor: use P\n* two motors in parallel: use P and P/S, \n  place jumpers across pins 1 & 2 and \n  3 & 4 on S\n* two motors in series: use P/S and S
$Comp
L power:+3.3V #PWR09
U 1 1 5C994897
P 2900 950
F 0 "#PWR09" H 2900 800 50  0001 C CNN
F 1 "+3.3V" H 2915 1123 50  0000 C CNN
F 2 "" H 2900 950 50  0001 C CNN
F 3 "" H 2900 950 50  0001 C CNN
	1    2900 950 
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR015
U 1 1 5C9950FF
P 3050 1150
F 0 "#PWR015" H 3050 1000 50  0001 C CNN
F 1 "+3.3V" H 3065 1323 50  0000 C CNN
F 2 "" H 3050 1150 50  0001 C CNN
F 3 "" H 3050 1150 50  0001 C CNN
	1    3050 1150
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR016
U 1 1 5C995CCA
P 2950 3250
F 0 "#PWR016" H 2950 3100 50  0001 C CNN
F 1 "+3.3V" H 2965 3423 50  0000 C CNN
F 2 "" H 2950 3250 50  0001 C CNN
F 3 "" H 2950 3250 50  0001 C CNN
	1    2950 3250
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR022
U 1 1 5C996833
P 3100 3450
F 0 "#PWR022" H 3100 3300 50  0001 C CNN
F 1 "+3.3V" H 3115 3623 50  0000 C CNN
F 2 "" H 3100 3450 50  0001 C CNN
F 3 "" H 3100 3450 50  0001 C CNN
	1    3100 3450
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR023
U 1 1 5C996C77
P 2950 5500
F 0 "#PWR023" H 2950 5350 50  0001 C CNN
F 1 "+3.3V" H 2965 5673 50  0000 C CNN
F 2 "" H 2950 5500 50  0001 C CNN
F 3 "" H 2950 5500 50  0001 C CNN
	1    2950 5500
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR025
U 1 1 5C9972F0
P 3100 5700
F 0 "#PWR025" H 3100 5550 50  0001 C CNN
F 1 "+3.3V" H 3115 5873 50  0000 C CNN
F 2 "" H 3100 5700 50  0001 C CNN
F 3 "" H 3100 5700 50  0001 C CNN
	1    3100 5700
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J45
U 1 1 5CA62FA6
P 1200 2250
F 0 "J45" H 1350 2250 50  0000 C CNN
F 1 "Conn_01x03" H 1118 2476 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1200 2250 50  0001 C CNN
F 3 "~" H 1200 2250 50  0001 C CNN
	1    1200 2250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1400 2250 2300 2250
Text GLabel 1400 4450 2    50   Output ~ 0
TMC_SDO
$Comp
L Connector_Generic:Conn_01x03 J47
U 1 1 5CA6ACF2
P 1200 4550
F 0 "J47" H 1350 4550 50  0000 C CNN
F 1 "Conn_01x03" H 1118 4776 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1200 4550 50  0001 C CNN
F 3 "~" H 1200 4550 50  0001 C CNN
	1    1200 4550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1400 4550 2350 4550
$Comp
L Connector_Generic:Conn_01x03 J46
U 1 1 5CA79CAF
P 1200 4250
F 0 "J46" H 1350 4250 50  0000 C CNN
F 1 "Conn_01x03" H 1118 4476 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1200 4250 50  0001 C CNN
F 3 "~" H 1200 4250 50  0001 C CNN
	1    1200 4250
	-1   0    0    1   
$EndComp
Wire Wire Line
	1400 4250 2350 4250
Wire Wire Line
	5000 4200 5100 4200
Wire Wire Line
	1400 2350 1400 4150
Text GLabel 1400 6600 2    50   Input ~ 0
TMC_SDI
Text GLabel 1400 6700 2    50   Output ~ 0
TMC_SDO
$Comp
L Connector_Generic:Conn_01x03 J49
U 1 1 5CA9AA3E
P 1200 6800
F 0 "J49" H 1350 6800 50  0000 C CNN
F 1 "Conn_01x03" H 1118 7026 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1200 6800 50  0001 C CNN
F 3 "~" H 1200 6800 50  0001 C CNN
	1    1200 6800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1400 6800 2350 6800
$Comp
L Connector_Generic:Conn_01x03 J48
U 1 1 5CA9AA49
P 1200 6500
F 0 "J48" H 1350 6500 50  0000 C CNN
F 1 "Conn_01x03" H 1118 6726 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1200 6500 50  0001 C CNN
F 3 "~" H 1200 6500 50  0001 C CNN
	1    1200 6500
	-1   0    0    1   
$EndComp
Wire Wire Line
	1400 6500 2350 6500
Connection ~ 4700 4500
Wire Wire Line
	1400 4650 1400 6400
Text GLabel 1400 6900 2    50   UnSpc ~ 0
TMC_SD_DC
$EndSCHEMATC
