EESchema Schematic File Version 4
LIBS:TeensyPrinter-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 9 10
Title "Teensy Pololu Shield"
Date "2019-03-28"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Linear:LM7805_TO220 U11
U 1 1 5CAF3130
P 6550 3700
F 0 "U11" H 6550 3942 50  0000 C CNN
F 1 "R-78E5.0-1.0" H 6550 3851 50  0000 C CNN
F 2 "vreg:Recom_R-78E-1.0" H 6550 3925 50  0001 C CIN
F 3 "https://www.recom-power.com/pdf/Innoline/R-78Exx-1.0.pdf" H 6550 3650 50  0001 C CNN
F 4 "Recom Power" H 6550 3700 50  0001 C CNN "DK_Mfr"
F 5 "R-78E5.0-1.0" H 6550 3700 50  0001 C CNN "DK_Mfr_PN"
F 6 "945-2201-ND" H 6550 3700 50  0001 C CNN "DK_PN"
F 7 "TO-220" H 0   0   50  0001 C CNN "Package"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6550 3700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR088
U 1 1 5CAF42C9
P 6550 4050
F 0 "#PWR088" H 6550 3800 50  0001 C CNN
F 1 "GND" H 6555 3877 50  0000 C CNN
F 2 "" H 6550 4050 50  0001 C CNN
F 3 "" H 6550 4050 50  0001 C CNN
	1    6550 4050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR087
U 1 1 5CAF43D1
P 6950 3700
F 0 "#PWR087" H 6950 3550 50  0001 C CNN
F 1 "+5V" H 6965 3873 50  0000 C CNN
F 2 "" H 6950 3700 50  0001 C CNN
F 3 "" H 6950 3700 50  0001 C CNN
	1    6950 3700
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR086
U 1 1 5CB45077
P 5700 3700
F 0 "#PWR086" H 5700 3550 50  0001 C CNN
F 1 "+12V" H 5715 3873 50  0000 C CNN
F 2 "" H 5700 3700 50  0001 C CNN
F 3 "" H 5700 3700 50  0001 C CNN
	1    5700 3700
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x06 J30
U 1 1 5CB45C7E
P 4450 4000
F 0 "J30" H 4368 3567 50  0000 C CNN
F 1 "Screw_Terminal_01x06" H 4368 3566 50  0001 C CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-6-5.08_1x06_P5.08mm_Horizontal" H 4450 4000 50  0001 C CNN
F 3 "~" H 4450 4000 50  0001 C CNN
F 4 "Phoenix Contact" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "1710726" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "277-17172-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4450 4000
	-1   0    0    1   
$EndComp
Wire Bus Line
	4050 3850 4350 3850
Wire Bus Line
	4350 4050 4050 4050
Text Notes 4350 3800 2    40   ~ 0
Motors/ +\nElectronics -
Text Notes 4350 4000 2    40   ~ 0
Hotends +\n-
Text Notes 4350 4200 2    40   ~ 0
Bed +\n-
$Comp
L power:GND #PWR090
U 1 1 5CB48113
P 4750 4300
F 0 "#PWR090" H 4750 4050 50  0001 C CNN
F 1 "GND" H 4755 4127 50  0000 C CNN
F 2 "" H 4750 4300 50  0001 C CNN
F 3 "" H 4750 4300 50  0001 C CNN
	1    4750 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4200 4750 4200
Wire Wire Line
	4750 4200 4750 4300
Wire Wire Line
	4750 4200 4750 4000
Wire Wire Line
	4750 4000 4650 4000
Connection ~ 4750 4200
Wire Wire Line
	4750 4000 4750 3800
Wire Wire Line
	4750 3800 4650 3800
Connection ~ 4750 4000
$Comp
L proper-passives:Fuse F1
U 1 1 5CB49EC4
P 5300 3700
F 0 "F1" H 5300 3987 60  0000 C CNN
F 1 "10A" H 5300 3881 60  0000 C CNN
F 2 "Fuse:Fuseholder_Blade_Mini_Keystone_3568" H 5350 3650 60  0001 C CNN
F 3 "" H 5350 3650 60  0001 C CNN
F 4 "Keystone" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "3568" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "36-3568-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5300 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 3700 5700 3700
Connection ~ 5700 3700
Wire Wire Line
	5000 3700 4650 3700
Wire Wire Line
	6550 4000 6550 4050
Wire Wire Line
	6850 3700 6900 3700
$Comp
L proper-passives:Fuse F2
U 1 1 5CB4AADA
P 5300 4150
F 0 "F2" H 5300 4437 60  0000 C CNN
F 1 "15A" H 5300 4331 60  0000 C CNN
F 2 "Fuse:Fuseholder_Blade_Mini_Keystone_3568" H 5350 4100 60  0001 C CNN
F 3 "" H 5350 4100 60  0001 C CNN
F 4 "Keystone" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "3568" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "36-3568-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5300 4150
	1    0    0    -1  
$EndComp
$Comp
L proper-passives:Fuse F3
U 1 1 5CB4B17A
P 5300 4600
F 0 "F3" H 5300 4887 60  0000 C CNN
F 1 "15A" H 5300 4781 60  0000 C CNN
F 2 "Fuse:Fuseholder_Blade_Mini_Keystone_3568" H 5350 4550 60  0001 C CNN
F 3 "" H 5350 4550 60  0001 C CNN
F 4 "Keystone" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "3568" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "36-3568-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5300 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 3900 5000 3900
Wire Wire Line
	5000 3900 5000 4150
Wire Wire Line
	4650 4100 4900 4100
Wire Wire Line
	4900 4100 4900 4600
Wire Wire Line
	4900 4600 5000 4600
$Comp
L power:+12VA #PWR089
U 1 1 5CB4BFF4
P 5700 4150
F 0 "#PWR089" H 5700 4000 50  0001 C CNN
F 1 "+12VA" H 5715 4323 50  0000 C CNN
F 2 "" H 5700 4150 50  0001 C CNN
F 3 "" H 5700 4150 50  0001 C CNN
	1    5700 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4150 5700 4150
$Comp
L power:+24V #PWR091
U 1 1 5CB4C560
P 5700 4600
F 0 "#PWR091" H 5700 4450 50  0001 C CNN
F 1 "+24V" H 5715 4773 50  0000 C CNN
F 2 "" H 5700 4600 50  0001 C CNN
F 3 "" H 5700 4600 50  0001 C CNN
	1    5700 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4600 5700 4600
Text Notes 4300 5300 0    50   ~ 0
Note: the +12V, +12VA, and +24V labels are arbitrary\nidentifiers for the motor/electronics, hotend, and bed\npower supplies, respectively.  Any combination of 12V\nand 24V can be used, according to the components your\nprinter uses.
$Comp
L Regulator_Linear:LM7805_TO220 U10
U 1 1 5CB53788
P 6550 2800
F 0 "U10" H 6550 3042 50  0000 C CNN
F 1 "R-78E3.3-1.0" H 6550 2951 50  0000 C CNN
F 2 "vreg:Recom_R-78E-1.0" H 6550 3025 50  0001 C CIN
F 3 "https://www.recom-power.com/pdf/Innoline/R-78Exx-1.0.pdf" H 6550 2750 50  0001 C CNN
F 4 "Recom Power" H 6550 2800 50  0001 C CNN "DK_Mfr"
F 5 "R-78E3.3-1.0" H 6550 2800 50  0001 C CNN "DK_Mfr_PN"
F 6 "945-2409-5-ND" H 6550 2800 50  0001 C CNN "DK_PN"
F 7 "TO-220" H 0   0   50  0001 C CNN "Package"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6550 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C15
U 1 1 5CB55316
P 6900 3000
F 0 "C15" H 7015 3046 50  0000 L CNN
F 1 "0.1u" H 7015 2955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6938 2850 50  0001 C CNN
F 3 "~" H 6900 3000 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "885012207098" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8080-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6900 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C14
U 1 1 5CB568CA
P 6200 3000
F 0 "C14" H 6000 3050 50  0000 L CNN
F 1 "0.1u" H 5900 2950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6238 2850 50  0001 C CNN
F 3 "~" H 6200 3000 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "885012207098" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8080-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6200 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 5CB5736A
P 6200 3900
F 0 "C16" H 6000 3950 50  0000 L CNN
F 1 "0.1u" H 5900 3850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6238 3750 50  0001 C CNN
F 3 "~" H 6200 3900 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "885012207098" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8080-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6200 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C17
U 1 1 5CB578D0
P 6900 3900
F 0 "C17" H 7015 3946 50  0000 L CNN
F 1 "0.1u" H 7015 3855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6938 3750 50  0001 C CNN
F 3 "~" H 6900 3900 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "885012207098" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8080-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6900 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3700 5900 3700
Wire Wire Line
	6200 3700 6200 3750
Connection ~ 6200 3700
Wire Wire Line
	6200 3700 6250 3700
Wire Wire Line
	6250 2800 6200 2800
Wire Wire Line
	6200 2800 6200 2850
Wire Wire Line
	6200 2800 5900 2800
Wire Wire Line
	5900 2800 5900 3700
Connection ~ 6200 2800
Connection ~ 5900 3700
Wire Wire Line
	5900 3700 6200 3700
Wire Wire Line
	6200 3150 6550 3150
$Comp
L power:GND #PWR085
U 1 1 5CB591C8
P 6550 3150
F 0 "#PWR085" H 6550 2900 50  0001 C CNN
F 1 "GND" H 6555 2977 50  0000 C CNN
F 2 "" H 6550 3150 50  0001 C CNN
F 3 "" H 6550 3150 50  0001 C CNN
	1    6550 3150
	1    0    0    -1  
$EndComp
Connection ~ 6550 3150
Wire Wire Line
	6550 3150 6900 3150
Wire Wire Line
	6550 3150 6550 3100
Wire Wire Line
	6850 2800 6900 2800
Wire Wire Line
	6900 2800 6900 2850
$Comp
L power:+3.3V #PWR084
U 1 1 5CB5A24D
P 6950 2800
F 0 "#PWR084" H 6950 2650 50  0001 C CNN
F 1 "+3.3V" H 6965 2973 50  0000 C CNN
F 2 "" H 6950 2800 50  0001 C CNN
F 3 "" H 6950 2800 50  0001 C CNN
	1    6950 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2800 6900 2800
Connection ~ 6900 2800
Wire Wire Line
	6900 3700 6900 3750
Connection ~ 6900 3700
Wire Wire Line
	6900 3700 6950 3700
Wire Wire Line
	6200 4050 6550 4050
Connection ~ 6550 4050
Wire Wire Line
	6550 4050 6900 4050
$Comp
L Device:LED_ALT D6
U 1 1 5CADD155
P 5700 2800
F 0 "D6" H 5700 2700 50  0000 C CNN
F 1 "POWER_LED" H 5693 2636 50  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 5700 2800 50  0001 C CNN
F 3 "~" H 5700 2800 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "150080RS75000" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-4984-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5700 2800
	1    0    0    1   
$EndComp
Wire Wire Line
	5850 2800 5900 2800
Connection ~ 5900 2800
$Comp
L Device:R_US R44
U 1 1 5CAE0648
P 5400 2800
F 0 "R44" V 5195 2800 50  0000 C CNN
F 1 "3.3k" V 5286 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5440 2790 50  0001 C CNN
F 3 "~" H 5400 2800 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT3K30" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT3K30CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5400 2800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR083
U 1 1 5CAE1052
P 5250 2800
F 0 "#PWR083" H 5250 2550 50  0001 C CNN
F 1 "GND" H 5255 2627 50  0000 C CNN
F 2 "" H 5250 2800 50  0001 C CNN
F 3 "" H 5250 2800 50  0001 C CNN
	1    5250 2800
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0115
U 1 1 5CBC7D50
P 7150 2800
F 0 "#PWR0115" H 7150 2650 50  0001 C CNN
F 1 "VCC" H 7167 2973 50  0000 C CNN
F 2 "" H 7150 2800 50  0001 C CNN
F 3 "" H 7150 2800 50  0001 C CNN
	1    7150 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 2800 6950 2800
Connection ~ 6950 2800
$EndSCHEMATC
