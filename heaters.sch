EESchema Schematic File Version 4
LIBS:TeensyPrinter-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 7 10
Title "Teensy Pololu Shield"
Date "2019-03-28"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 6550 3000 0    50   Input ~ 0
HEATER_BED_B
Text GLabel 2300 2900 0    50   Input ~ 0
HEATER_E0_B
Text GLabel 2300 4200 0    50   Input ~ 0
HEATER_E1_B
Text GLabel 2300 5500 0    50   Input ~ 0
HEATER_E2_B
$Comp
L 74xx:74LCX07 U9
U 1 1 5CAE9672
P 6850 3000
F 0 "U9" H 6850 3317 50  0000 C CNN
F 1 "74LCX07" H 6850 3226 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 6850 3000 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 6850 3000 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6850 3000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U9
U 2 1 5CAEA4A4
P 2600 2900
F 0 "U9" H 2600 3217 50  0000 C CNN
F 1 "74LCX07" H 2600 3126 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2600 2900 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 2600 2900 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	2    2600 2900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U9
U 3 1 5CAEB0C6
P 2600 4200
F 0 "U9" H 2600 4517 50  0000 C CNN
F 1 "74LCX07" H 2600 4426 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2600 4200 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 2600 4200 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	3    2600 4200
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U9
U 4 1 5CAEC8F4
P 2600 5500
F 0 "U9" H 2600 5817 50  0000 C CNN
F 1 "74LCX07" H 2600 5726 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 2600 5500 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 2600 5500 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	4    2600 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q2
U 1 1 5C9E8D12
P 8000 3000
F 0 "Q2" H 8206 3046 50  0000 L CNN
F 1 "IRLS3034" H 8206 2955 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-263-2" H 8200 3100 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/irls3034pbf.pdf?fileId=5546d462533600a401535671b7472707" H 8000 3000 50  0001 C CNN
F 4 "Infineon" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "IRLS3034TRLPBF" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "IRLS3034TRLPBFCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "TO-263-2" H 0   0   50  0001 C CNN "Package"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    8000 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R29
U 1 1 5C9EA8EE
P 7500 3000
F 0 "R29" V 7295 3000 50  0000 C CNN
F 1 "100" V 7386 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7540 2990 50  0001 C CNN
F 3 "~" H 7500 3000 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100R" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100RCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    7500 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R31
U 1 1 5C9EB884
P 7850 3300
F 0 "R31" V 7645 3300 50  0000 C CNN
F 1 "100k" V 7736 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7890 3290 50  0001 C CNN
F 3 "~" H 7850 3300 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    7850 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	7650 3000 7700 3000
Wire Wire Line
	8100 3300 8100 3200
$Comp
L power:GND #PWR073
U 1 1 5C9EDF53
P 8100 3300
F 0 "#PWR073" H 8100 3050 50  0001 C CNN
F 1 "GND" H 8105 3127 50  0000 C CNN
F 2 "" H 8100 3300 50  0001 C CNN
F 3 "" H 8100 3300 50  0001 C CNN
	1    8100 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R27
U 1 1 5C9EE0DD
P 7250 2750
F 0 "R27" H 7050 2800 50  0000 L CNN
F 1 "1k" H 7100 2700 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 7290 2740 50  0001 C CNN
F 3 "~" H 7250 2750 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT1K00" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT1K00CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    7250 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3000 7250 3000
Wire Wire Line
	7250 2900 7250 3000
Connection ~ 7250 3000
Wire Wire Line
	7250 3000 7350 3000
$Comp
L power:+5V #PWR071
U 1 1 5C9EFEC5
P 7250 2600
F 0 "#PWR071" H 7250 2450 50  0001 C CNN
F 1 "+5V" H 7265 2773 50  0000 C CNN
F 2 "" H 7250 2600 50  0001 C CNN
F 3 "" H 7250 2600 50  0001 C CNN
	1    7250 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J27
U 1 1 5CA7F8C1
P 9000 2650
F 0 "J27" H 8950 2750 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 9080 2551 50  0001 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_MKDS-1,5-2-5.08_1x02_P5.08mm_Horizontal" H 9000 2650 50  0001 C CNN
F 3 "~" H 9000 2650 50  0001 C CNN
F 4 "Phoenix Contact" H 0   100 50  0001 C CNN "DK_Mfr"
F 5 "1985904" H 0   100 50  0001 C CNN "DK_Mfr_PN"
F 6 "	277-14505-ND" H 0   100 50  0001 C CNN "DK_PN"
F 7 "any" H 0   100 50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   100 50  0001 C CNN "Qty Per Unit"
	1    9000 2650
	1    0    0    1   
$EndComp
$Comp
L Device:LED_ALT D2
U 1 1 5CA81080
P 8250 2550
F 0 "D2" H 8250 2400 50  0000 C CNN
F 1 "HEATER_BED_LED" H 8243 2386 50  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 8250 2550 50  0001 C CNN
F 3 "~" H 8250 2550 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "150080RS75000" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-4984-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    8250 2550
	1    0    0    1   
$EndComp
$Comp
L Device:R_US R25
U 1 1 5CA8186E
P 8550 2550
F 0 "R25" V 8345 2550 50  0000 C CNN
F 1 "3.3k" V 8436 2550 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8590 2540 50  0001 C CNN
F 3 "~" H 8550 2550 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT3K30" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT3K30CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    8550 2550
	0    1    1    0   
$EndComp
$Comp
L power:+24V #PWR070
U 1 1 5CA82ABD
P 8750 2500
F 0 "#PWR070" H 8750 2350 50  0001 C CNN
F 1 "+24V" H 8765 2673 50  0000 C CNN
F 2 "" H 8750 2500 50  0001 C CNN
F 3 "" H 8750 2500 50  0001 C CNN
	1    8750 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 2550 8750 2550
Wire Wire Line
	8750 2550 8750 2500
Connection ~ 8750 2550
Wire Wire Line
	8750 2550 8800 2550
Wire Wire Line
	8100 2550 8100 2650
Wire Wire Line
	8800 2650 8100 2650
Connection ~ 8100 2650
Wire Wire Line
	8100 2650 8100 2800
$Comp
L Device:Q_NMOS_GDS Q1
U 1 1 5CA8D0BD
P 3750 2900
F 0 "Q1" H 3956 2946 50  0000 L CNN
F 1 "IRLR8743" H 3956 2855 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 3950 3000 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/irlr8743pbf.pdf?fileId=5546d462533600a4015356719c7e26ff" H 3750 2900 50  0001 C CNN
F 4 "Infineon" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "IRLR8743TRPBF" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "IRLR8743TRPBFCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "TO-252-2" H 0   0   50  0001 C CNN "Package"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3750 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R28
U 1 1 5CA8D0C7
P 3250 2900
F 0 "R28" V 3045 2900 50  0000 C CNN
F 1 "100" V 3136 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3290 2890 50  0001 C CNN
F 3 "~" H 3250 2900 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100R" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100RCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3250 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R30
U 1 1 5CA8D0D1
P 3600 3200
F 0 "R30" V 3395 3200 50  0000 C CNN
F 1 "100k" V 3486 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3640 3190 50  0001 C CNN
F 3 "~" H 3600 3200 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3600 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 2900 3450 2900
Wire Wire Line
	3850 3200 3850 3100
$Comp
L power:GND #PWR072
U 1 1 5CA8D0E0
P 3850 3200
F 0 "#PWR072" H 3850 2950 50  0001 C CNN
F 1 "GND" H 3855 3027 50  0000 C CNN
F 2 "" H 3850 3200 50  0001 C CNN
F 3 "" H 3850 3200 50  0001 C CNN
	1    3850 3200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R26
U 1 1 5CA8D0EB
P 3000 2650
F 0 "R26" H 2800 2700 50  0000 L CNN
F 1 "1k" H 2850 2600 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3040 2640 50  0001 C CNN
F 3 "~" H 3000 2650 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT1K00" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT1K00CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3000 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2900 3000 2900
Wire Wire Line
	3000 2800 3000 2900
Connection ~ 3000 2900
Wire Wire Line
	3000 2900 3100 2900
$Comp
L power:+5V #PWR069
U 1 1 5CA8D0F9
P 3000 2500
F 0 "#PWR069" H 3000 2350 50  0001 C CNN
F 1 "+5V" H 3015 2673 50  0000 C CNN
F 2 "" H 3000 2500 50  0001 C CNN
F 3 "" H 3000 2500 50  0001 C CNN
	1    3000 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_ALT D1
U 1 1 5CA8D10D
P 4000 2450
F 0 "D1" H 4000 2300 50  0000 C CNN
F 1 "HEATER_E0_LED" H 3993 2286 50  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4000 2450 50  0001 C CNN
F 3 "~" H 4000 2450 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "150080RS75000" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-4984-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4000 2450
	1    0    0    1   
$EndComp
$Comp
L Device:R_US R24
U 1 1 5CA8D117
P 4300 2450
F 0 "R24" V 4095 2450 50  0000 C CNN
F 1 "3.3k" V 4186 2450 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4340 2440 50  0001 C CNN
F 3 "~" H 4300 2450 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT3K30" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT3K30CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4300 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 2450 3850 2550
Connection ~ 3850 2550
Wire Wire Line
	3850 2550 3850 2700
$Comp
L power:+12VA #PWR068
U 1 1 5CA940C2
P 4800 2450
F 0 "#PWR068" H 4800 2300 50  0001 C CNN
F 1 "+12VA" H 4815 2623 50  0000 C CNN
F 2 "" H 4800 2450 50  0001 C CNN
F 3 "" H 4800 2450 50  0001 C CNN
	1    4800 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GDS Q4
U 1 1 5CA94636
P 3750 5500
F 0 "Q4" H 3956 5546 50  0000 L CNN
F 1 "IRLR8743" H 3956 5455 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 3950 5600 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/irlr8743pbf.pdf?fileId=5546d462533600a4015356719c7e26ff" H 3750 5500 50  0001 C CNN
F 4 "Infineon" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "IRLR8743TRPBF" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "IRLR8743TRPBFCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "TO-252-2" H 0   0   50  0001 C CNN "Package"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3750 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R38
U 1 1 5CA94640
P 3250 5500
F 0 "R38" V 3045 5500 50  0000 C CNN
F 1 "100" V 3136 5500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3290 5490 50  0001 C CNN
F 3 "~" H 3250 5500 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100R" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100RCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3250 5500
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R39
U 1 1 5CA9464A
P 3600 5800
F 0 "R39" V 3395 5800 50  0000 C CNN
F 1 "100k" V 3486 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3640 5790 50  0001 C CNN
F 3 "~" H 3600 5800 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3600 5800
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 5500 3450 5500
Wire Wire Line
	3850 5800 3850 5700
$Comp
L power:GND #PWR079
U 1 1 5CA94659
P 3850 5800
F 0 "#PWR079" H 3850 5550 50  0001 C CNN
F 1 "GND" H 3855 5627 50  0000 C CNN
F 2 "" H 3850 5800 50  0001 C CNN
F 3 "" H 3850 5800 50  0001 C CNN
	1    3850 5800
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R37
U 1 1 5CA94664
P 3000 5250
F 0 "R37" H 2800 5300 50  0000 L CNN
F 1 "1k" H 2850 5200 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3040 5240 50  0001 C CNN
F 3 "~" H 3000 5250 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT1K00" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT1K00CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3000 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 5500 3000 5500
Wire Wire Line
	3000 5400 3000 5500
Connection ~ 3000 5500
Wire Wire Line
	3000 5500 3100 5500
$Comp
L power:+5V #PWR077
U 1 1 5CA94672
P 3000 5100
F 0 "#PWR077" H 3000 4950 50  0001 C CNN
F 1 "+5V" H 3015 5273 50  0000 C CNN
F 2 "" H 3000 5100 50  0001 C CNN
F 3 "" H 3000 5100 50  0001 C CNN
	1    3000 5100
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_ALT D4
U 1 1 5CA94686
P 4000 5050
F 0 "D4" H 4000 4900 50  0000 C CNN
F 1 "HEATER_E2_LED" H 3993 4886 50  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4000 5050 50  0001 C CNN
F 3 "~" H 4000 5050 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "150080RS75000" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-4984-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4000 5050
	1    0    0    1   
$EndComp
$Comp
L Device:R_US R36
U 1 1 5CA94690
P 4300 5050
F 0 "R36" V 4095 5050 50  0000 C CNN
F 1 "3.3k" V 4186 5050 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4340 5040 50  0001 C CNN
F 3 "~" H 4300 5050 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT3K30" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT3K30CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4300 5050
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 5050 3850 5150
Connection ~ 3850 5150
Wire Wire Line
	3850 5150 3850 5300
$Comp
L Device:Q_NMOS_GDS Q3
U 1 1 5CA9EC6C
P 3750 4200
F 0 "Q3" H 3956 4246 50  0000 L CNN
F 1 "IRLR8743" H 3956 4155 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 3950 4300 50  0001 C CNN
F 3 "https://www.infineon.com/dgdl/irlr8743pbf.pdf?fileId=5546d462533600a4015356719c7e26ff" H 3750 4200 50  0001 C CNN
F 4 "Infineon" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "IRLR8743TRPBF" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "IRLR8743TRPBFCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "TO-252-2" H 0   0   50  0001 C CNN "Package"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3750 4200
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R34
U 1 1 5CA9EC76
P 3250 4200
F 0 "R34" V 3045 4200 50  0000 C CNN
F 1 "100" V 3136 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3290 4190 50  0001 C CNN
F 3 "~" H 3250 4200 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100R" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100RCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3250 4200
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R35
U 1 1 5CA9EC80
P 3600 4500
F 0 "R35" V 3395 4500 50  0000 C CNN
F 1 "100k" V 3486 4500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3640 4490 50  0001 C CNN
F 3 "~" H 3600 4500 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3600 4500
	0    1    1    0   
$EndComp
Wire Wire Line
	3400 4200 3450 4200
Wire Wire Line
	3850 4500 3850 4400
$Comp
L power:GND #PWR076
U 1 1 5CA9EC8F
P 3850 4500
F 0 "#PWR076" H 3850 4250 50  0001 C CNN
F 1 "GND" H 3855 4327 50  0000 C CNN
F 2 "" H 3850 4500 50  0001 C CNN
F 3 "" H 3850 4500 50  0001 C CNN
	1    3850 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R33
U 1 1 5CA9EC9A
P 3000 3950
F 0 "R33" H 2800 4000 50  0000 L CNN
F 1 "1k" H 2850 3900 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3040 3940 50  0001 C CNN
F 3 "~" H 3000 3950 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT1K00" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT1K00CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    3000 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 4200 3000 4200
Wire Wire Line
	3000 4100 3000 4200
Connection ~ 3000 4200
Wire Wire Line
	3000 4200 3100 4200
$Comp
L power:+5V #PWR074
U 1 1 5CA9ECA8
P 3000 3800
F 0 "#PWR074" H 3000 3650 50  0001 C CNN
F 1 "+5V" H 3015 3973 50  0000 C CNN
F 2 "" H 3000 3800 50  0001 C CNN
F 3 "" H 3000 3800 50  0001 C CNN
	1    3000 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_ALT D3
U 1 1 5CA9ECBC
P 4000 3750
F 0 "D3" H 4000 3600 50  0000 C CNN
F 1 "HEATER_E1_LED" H 3993 3586 50  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 4000 3750 50  0001 C CNN
F 3 "~" H 4000 3750 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "150080RS75000" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-4984-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4000 3750
	1    0    0    1   
$EndComp
$Comp
L Device:R_US R32
U 1 1 5CA9ECC6
P 4300 3750
F 0 "R32" V 4095 3750 50  0000 C CNN
F 1 "3.3k" V 4186 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4340 3740 50  0001 C CNN
F 3 "~" H 4300 3750 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT3K30" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT3K30CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4300 3750
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 3750 3850 3850
Connection ~ 3850 3850
Wire Wire Line
	3850 3850 3850 4000
$Comp
L Connector:Screw_Terminal_01x06 J28
U 1 1 5CACF86A
P 5100 3750
F 0 "J28" H 5050 4100 50  0000 L CNN
F 1 "Screw_Terminal_01x06" H 5180 3651 50  0001 L CNN
F 2 "TerminalBlock_Phoenix:TerminalBlock_Phoenix_PT-1,5-6-3.5-H_1x06_P3.50mm_Horizontal" H 5100 3750 50  0001 C CNN
F 3 "~" H 5100 3750 50  0001 C CNN
F 4 "Phoenix Contact" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "1984659" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "277-1724-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5100 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3850 4900 3850
Wire Wire Line
	4850 2550 4850 3650
Wire Wire Line
	4850 3650 4900 3650
Wire Wire Line
	3850 2550 4850 2550
Wire Wire Line
	4800 2450 4800 3550
Wire Wire Line
	4800 3550 4900 3550
Connection ~ 4800 2450
Wire Wire Line
	4450 2450 4800 2450
Wire Wire Line
	4450 3750 4800 3750
Wire Wire Line
	4800 3550 4800 3750
Connection ~ 4800 3550
Connection ~ 4800 3750
Wire Wire Line
	4800 3750 4900 3750
Wire Wire Line
	4800 3750 4800 3950
Wire Wire Line
	4800 3950 4900 3950
Wire Wire Line
	4800 3950 4800 5050
Wire Wire Line
	4450 5050 4800 5050
Connection ~ 4800 3950
Wire Wire Line
	4900 5150 4900 4050
Wire Wire Line
	3850 5150 4900 5150
$Comp
L 74xx:74LCX07 U9
U 7 1 5CAE214D
P 7700 4800
F 0 "U9" H 7930 4846 50  0000 L CNN
F 1 "74LCX07" H 7930 4755 50  0000 L CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 7700 4800 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 7700 4800 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	7    7700 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 5CAE32D5
P 7150 4800
F 0 "C13" H 7265 4846 50  0000 L CNN
F 1 "0.1u" H 7265 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7188 4650 50  0001 C CNN
F 3 "~" H 7150 4800 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "885012207098" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-8080-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    7150 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 4650 7150 4300
Wire Wire Line
	7150 4300 7700 4300
Wire Wire Line
	7150 4950 7150 5300
Wire Wire Line
	7150 5300 7700 5300
$Comp
L power:GND #PWR078
U 1 1 5CAE63BB
P 7700 5300
F 0 "#PWR078" H 7700 5050 50  0001 C CNN
F 1 "GND" H 7705 5127 50  0000 C CNN
F 2 "" H 7700 5300 50  0001 C CNN
F 3 "" H 7700 5300 50  0001 C CNN
	1    7700 5300
	1    0    0    -1  
$EndComp
Connection ~ 7700 5300
$Comp
L power:+5V #PWR075
U 1 1 5CAE6743
P 7700 4300
F 0 "#PWR075" H 7700 4150 50  0001 C CNN
F 1 "+5V" H 7715 4473 50  0000 C CNN
F 2 "" H 7700 4300 50  0001 C CNN
F 3 "" H 7700 4300 50  0001 C CNN
	1    7700 4300
	1    0    0    -1  
$EndComp
Connection ~ 7700 4300
Wire Wire Line
	3450 3200 3450 2900
Connection ~ 3450 2900
Wire Wire Line
	3450 2900 3550 2900
Wire Wire Line
	3750 3200 3850 3200
Connection ~ 3850 3200
Wire Wire Line
	3450 4500 3450 4200
Connection ~ 3450 4200
Wire Wire Line
	3450 4200 3550 4200
Wire Wire Line
	3750 4500 3850 4500
Connection ~ 3850 4500
Wire Wire Line
	3450 5800 3450 5500
Connection ~ 3450 5500
Wire Wire Line
	3450 5500 3550 5500
Wire Wire Line
	3750 5800 3850 5800
Connection ~ 3850 5800
Wire Wire Line
	7700 3300 7700 3000
Connection ~ 7700 3000
Wire Wire Line
	7700 3000 7800 3000
Wire Wire Line
	8000 3300 8100 3300
Connection ~ 8100 3300
Wire Bus Line
	5200 3700 5400 3700
Wire Bus Line
	5200 3900 5400 3900
Text Notes 5200 3650 0    47   ~ 0
+ E0\n-
Text Notes 5200 3850 0    47   ~ 0
+ E1\n-
Text Notes 5200 4050 0    47   ~ 0
+ E2\n-
Text Notes 9100 2650 0    47   ~ 0
+\n-
$EndSCHEMATC
