EESchema Schematic File Version 4
LIBS:TeensyPrinter-cache
EELAYER 29 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 8 10
Title "Teensy Pololu Shield"
Date "2019-03-28"
Rev "0.1"
Comp "Scott Alfter"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 4250 2400 0    50   Input ~ 0
FAN
$Comp
L Device:R_US R42
U 1 1 5CAF3B14
P 5200 2400
F 0 "R42" V 4995 2400 50  0000 C CNN
F 1 "100" V 5086 2400 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5240 2390 50  0001 C CNN
F 3 "~" H 5200 2400 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100R" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100RCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5200 2400
	0    1    1    0   
$EndComp
$Comp
L Device:R_US R43
U 1 1 5CAF3B1A
P 5550 2700
F 0 "R43" V 5345 2700 50  0000 C CNN
F 1 "100k" V 5436 2700 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5590 2690 50  0001 C CNN
F 3 "~" H 5550 2700 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT100K" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT100KCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5550 2700
	0    1    1    0   
$EndComp
Wire Wire Line
	5350 2400 5400 2400
Wire Wire Line
	5800 2700 5800 2600
$Comp
L power:GND #PWR082
U 1 1 5CAF3B25
P 5800 2700
F 0 "#PWR082" H 5800 2450 50  0001 C CNN
F 1 "GND" H 5805 2527 50  0000 C CNN
F 2 "" H 5800 2700 50  0001 C CNN
F 3 "" H 5800 2700 50  0001 C CNN
	1    5800 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R41
U 1 1 5CAF3B2C
P 4950 2150
F 0 "R41" H 4750 2200 50  0000 L CNN
F 1 "1k" H 4800 2100 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4990 2140 50  0001 C CNN
F 3 "~" H 4950 2150 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT1K00" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT1K00CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4950 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 2400 4950 2400
Wire Wire Line
	4950 2300 4950 2400
Connection ~ 4950 2400
Wire Wire Line
	4950 2400 5050 2400
$Comp
L power:+5V #PWR081
U 1 1 5CAF3B36
P 4950 2000
F 0 "#PWR081" H 4950 1850 50  0001 C CNN
F 1 "+5V" H 4965 2173 50  0000 C CNN
F 2 "" H 4950 2000 50  0001 C CNN
F 3 "" H 4950 2000 50  0001 C CNN
	1    4950 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:LED_ALT D5
U 1 1 5CAF3B42
P 5950 1950
F 0 "D5" H 5950 1800 50  0000 C CNN
F 1 "FAN_LED" H 5943 1786 50  0001 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 5950 1950 50  0001 C CNN
F 3 "~" H 5950 1950 50  0001 C CNN
F 4 "Würth" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "150080RS75000" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "732-4984-1-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5950 1950
	1    0    0    1   
$EndComp
$Comp
L Device:R_US R40
U 1 1 5CAF3B48
P 6250 1950
F 0 "R40" V 6045 1950 50  0000 C CNN
F 1 "3.3k" V 6136 1950 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6290 1940 50  0001 C CNN
F 3 "~" H 6250 1950 50  0001 C CNN
F 4 "Stackpole " H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "RMCF0805FT3K30" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "RMCF0805FT3K30CT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "0805" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6250 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 1950 6450 1950
Wire Wire Line
	6450 1950 6450 1900
Connection ~ 6450 1950
Wire Wire Line
	6450 1950 6500 1950
Wire Wire Line
	5800 1950 5800 2050
Wire Wire Line
	6500 2050 5800 2050
Connection ~ 5800 2050
Wire Wire Line
	5800 2050 5800 2200
$Comp
L 74xx:74LCX07 U9
U 5 1 5CB0EA27
P 4550 2400
F 0 "U9" H 4550 2717 50  0000 C CNN
F 1 "74LCX07" H 4550 2626 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 4550 2400 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 4550 2400 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	5    4550 2400
	1    0    0    -1  
$EndComp
Text GLabel 4450 3800 0    50   Input ~ 0
SERVO_0_B
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J31
U 1 1 5CB2DA68
P 4750 3700
F 0 "J31" H 4800 3925 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4800 3926 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 4750 3700 50  0001 C CNN
F 3 "~" H 4750 3700 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "67997-206HLF" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "609-3234-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4750 3700
	1    0    0    -1  
$EndComp
Text GLabel 5150 3800 2    50   Input ~ 0
SERVO_1_B
Wire Wire Line
	5050 3800 5150 3800
Wire Wire Line
	4450 3800 4550 3800
$Comp
L power:GND #PWR095
U 1 1 5CB2F6EB
P 4800 3950
F 0 "#PWR095" H 4800 3700 50  0001 C CNN
F 1 "GND" H 4805 3777 50  0000 C CNN
F 2 "" H 4800 3950 50  0001 C CNN
F 3 "" H 4800 3950 50  0001 C CNN
	1    4800 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3950 4500 3950
Wire Wire Line
	4500 3950 4500 3700
Wire Wire Line
	4500 3700 4550 3700
Wire Wire Line
	4800 3950 5100 3950
Wire Wire Line
	5100 3950 5100 3700
Wire Wire Line
	5100 3700 5050 3700
Connection ~ 4800 3950
$Comp
L power:+3.3V #PWR093
U 1 1 5CB30241
P 4800 3400
F 0 "#PWR093" H 4800 3250 50  0001 C CNN
F 1 "+3.3V" H 4815 3573 50  0000 C CNN
F 2 "" H 4800 3400 50  0001 C CNN
F 3 "" H 4800 3400 50  0001 C CNN
	1    4800 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3400 5100 3400
Wire Wire Line
	5100 3400 5100 3600
Wire Wire Line
	5100 3600 5050 3600
Wire Wire Line
	4800 3400 4500 3400
Wire Wire Line
	4500 3400 4500 3600
Wire Wire Line
	4500 3600 4550 3600
Connection ~ 4800 3400
Text GLabel 4450 4850 0    50   Input ~ 0
SOL_0_B
Wire Wire Line
	4450 4850 4550 4850
$Comp
L power:GND #PWR098
U 1 1 5CB32509
P 4500 5000
F 0 "#PWR098" H 4500 4750 50  0001 C CNN
F 1 "GND" H 4505 4827 50  0000 C CNN
F 2 "" H 4500 5000 50  0001 C CNN
F 3 "" H 4500 5000 50  0001 C CNN
	1    4500 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 5000 4500 4750
Wire Wire Line
	4500 4750 4550 4750
$Comp
L power:+3.3V #PWR097
U 1 1 5CB3251A
P 4500 4450
F 0 "#PWR097" H 4500 4300 50  0001 C CNN
F 1 "+3.3V" H 4515 4623 50  0000 C CNN
F 2 "" H 4500 4450 50  0001 C CNN
F 3 "" H 4500 4450 50  0001 C CNN
	1    4500 4450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 4450 4500 4650
Wire Wire Line
	4500 4650 4550 4650
Text GLabel 6050 3800 0    50   Input ~ 0
PS_ON
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J32
U 1 1 5CB34642
P 6350 3700
F 0 "J32" H 6400 3925 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 6400 3926 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 6350 3700 50  0001 C CNN
F 3 "~" H 6350 3700 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "67997-206HLF" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "609-3234-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6350 3700
	1    0    0    -1  
$EndComp
Text GLabel 6750 3800 2    50   Input ~ 0
FILWIDTH
Wire Wire Line
	6650 3800 6750 3800
Wire Wire Line
	6050 3800 6150 3800
$Comp
L power:GND #PWR096
U 1 1 5CB3464F
P 6400 3950
F 0 "#PWR096" H 6400 3700 50  0001 C CNN
F 1 "GND" H 6405 3777 50  0000 C CNN
F 2 "" H 6400 3950 50  0001 C CNN
F 3 "" H 6400 3950 50  0001 C CNN
	1    6400 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3950 6100 3950
Wire Wire Line
	6100 3950 6100 3700
Wire Wire Line
	6100 3700 6150 3700
Wire Wire Line
	6400 3950 6700 3950
Wire Wire Line
	6700 3950 6700 3700
Wire Wire Line
	6700 3700 6650 3700
Connection ~ 6400 3950
$Comp
L power:+3.3V #PWR094
U 1 1 5CB34660
P 6400 3400
F 0 "#PWR094" H 6400 3250 50  0001 C CNN
F 1 "+3.3V" H 6415 3573 50  0000 C CNN
F 2 "" H 6400 3400 50  0001 C CNN
F 3 "" H 6400 3400 50  0001 C CNN
	1    6400 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3400 6700 3400
Wire Wire Line
	6700 3400 6700 3600
Wire Wire Line
	6700 3600 6650 3600
Wire Wire Line
	6400 3400 6100 3400
Wire Wire Line
	6100 3400 6100 3600
Wire Wire Line
	6100 3600 6150 3600
Connection ~ 6400 3400
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J34
U 1 1 5CB39B5E
P 6400 4750
F 0 "J34" H 6450 4975 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 6450 4976 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 6400 4750 50  0001 C CNN
F 3 "~" H 6400 4750 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "67997-206HLF" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "609-3234-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6400 4750
	1    0    0    -1  
$EndComp
Text GLabel 6200 4650 0    50   Input ~ 0
SD_SDI
Text GLabel 6200 4750 0    50   Output ~ 0
SD_SDO
Text GLabel 6200 4850 0    50   Input ~ 0
SD_SCK
Text GLabel 6700 4650 2    50   Input ~ 0
TMC_SDI
Text GLabel 6700 4850 2    50   Input ~ 0
TMC_SCK
Text GLabel 6700 4750 2    50   Output ~ 0
TMC_SDO
$Comp
L power:+12V #PWR099
U 1 1 5CB4BB42
P 5450 5400
F 0 "#PWR099" H 5450 5250 50  0001 C CNN
F 1 "+12V" H 5465 5573 50  0000 C CNN
F 2 "" H 5450 5400 50  0001 C CNN
F 3 "" H 5450 5400 50  0001 C CNN
	1    5450 5400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LCX07 U9
U 6 1 5CB5AE6D
P 8550 2400
F 0 "U9" H 8550 2717 50  0000 C CNN
F 1 "74LCX07" H 8550 2626 50  0000 C CNN
F 2 "Package_SO:SOIC-14_3.9x8.7mm_P1.27mm" H 8550 2400 50  0001 C CNN
F 3 "http://www.onsemi.com/pub/Collateral/MC74LCX07-D.PDF" H 8550 2400 50  0001 C CNN
F 4 "ON Semiconductor" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "MC74LCX07DR2G" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "MC74LCX07DR2GOSCT-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "14-SOIC" H 0   0   50  0001 C CNN "Package"
F 8 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 9 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	6    8550 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR092
U 1 1 5CB5C160
P 8250 2400
F 0 "#PWR092" H 8250 2150 50  0001 C CNN
F 1 "GND" H 8255 2227 50  0000 C CNN
F 2 "" H 8250 2400 50  0001 C CNN
F 3 "" H 8250 2400 50  0001 C CNN
	1    8250 2400
	1    0    0    -1  
$EndComp
NoConn ~ 8850 2400
$Comp
L Connector_Generic:Conn_01x02 J29
U 1 1 5CB5DC89
P 6700 1950
F 0 "J29" H 6650 2050 50  0000 L CNN
F 1 "Conn_01x02" H 6780 1851 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 6700 1950 50  0001 C CNN
F 3 "~" H 6700 1950 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "77311-818-02LF" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "609-4938-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    6700 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 2700 5800 2700
Connection ~ 5800 2700
Wire Wire Line
	5400 2700 5400 2400
Connection ~ 5400 2400
Wire Wire Line
	5400 2400 5500 2400
Text Notes 5650 5250 0    47   ~ 0
Acc Pwr
Text Notes 6400 4450 0    47   ~ 0
SPI
Text Notes 6800 2050 0    47   ~ 0
+\n-
$Comp
L Connector_Generic:Conn_01x03 J33
U 1 1 5CB7BC9C
P 4750 4750
F 0 "J33" H 4830 4746 50  0000 L CNN
F 1 "Conn_01x03" H 4830 4701 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4750 4750 50  0001 C CNN
F 3 "~" H 4750 4750 50  0001 C CNN
F 4 "METZ CONNECT USA Inc." H 0   0   50  0001 C CNN "DK_Mfr"
F 5 "PR20203VBNN" H 0   0   50  0001 C CNN "DK_Mfr_PN"
F 6 "1849-1003-ND" H 0   0   50  0001 C CNN "DK_PN"
F 7 "any" H 0   0   50  0001 C CNN "Src Any/Spec"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    4750 4750
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NMOS_GSD Q5
U 1 1 5CBF3D8E
P 5700 2400
F 0 "Q5" H 5906 2446 50  0000 L CNN
F 1 "ZXMN3F30FH" H 5906 2355 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5900 2500 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/ZXMN3F30FH.pdf" H 5700 2400 50  0001 C CNN
F 4 "Diodes Incorporated" H 5700 2400 50  0001 C CNN "DK_Mfr"
F 5 "ZXMN3F30FHTA" H 5700 2400 50  0001 C CNN "DK_Mfr_PN"
F 6 "ZXMN3F30FHCT-ND" H 5700 2400 50  0001 C CNN "DK_PN"
F 7 "SOT23" H 5700 2400 50  0001 C CNN "Package"
F 8 "1" H 0   0   50  0001 C CNN "Qty Per Unit"
	1    5700 2400
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0122
U 1 1 5C9B0326
P 6450 1900
F 0 "#PWR0122" H 6450 1750 50  0001 C CNN
F 1 "+12V" H 6465 2073 50  0000 C CNN
F 2 "" H 6450 1900 50  0001 C CNN
F 3 "" H 6450 1900 50  0001 C CNN
	1    6450 1900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0100
U 1 1 5CB4BE89
P 5950 6000
F 0 "#PWR0100" H 5950 5750 50  0001 C CNN
F 1 "GND" H 5955 5827 50  0000 C CNN
F 2 "" H 5950 6000 50  0001 C CNN
F 3 "" H 5950 6000 50  0001 C CNN
	1    5950 6000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J35
U 1 1 5C9BFD78
P 5650 5650
F 0 "J35" H 5700 5975 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 5700 5976 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x06_P2.54mm_Vertical" H 5650 5650 50  0001 C CNN
F 3 "~" H 5650 5650 50  0001 C CNN
	1    5650 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 5400 5450 5450
Connection ~ 5450 5450
Wire Wire Line
	5450 5450 5450 5550
Connection ~ 5450 5550
Wire Wire Line
	5450 5550 5450 5650
Connection ~ 5450 5650
Wire Wire Line
	5450 5650 5450 5750
Connection ~ 5450 5750
Wire Wire Line
	5450 5750 5450 5850
Connection ~ 5450 5850
Wire Wire Line
	5450 5850 5450 5950
Wire Wire Line
	5950 5450 5950 5550
Connection ~ 5950 5550
Wire Wire Line
	5950 5550 5950 5650
Connection ~ 5950 5650
Wire Wire Line
	5950 5650 5950 5750
Connection ~ 5950 5750
Wire Wire Line
	5950 5750 5950 5850
Connection ~ 5950 5850
Wire Wire Line
	5950 5850 5950 5950
Connection ~ 5950 5950
Wire Wire Line
	5950 5950 5950 6000
$EndSCHEMATC
